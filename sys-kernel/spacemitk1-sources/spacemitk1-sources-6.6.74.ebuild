# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
ETYPE="sources"
K_WANT_GENPATCHES="base extras experimental"
K_GENPATCHES_VER="83"

inherit kernel-2
detect_version
detect_arch

DESCRIPTION="Full sources including the Gentoo/spacemitk1 patchset for the ${KV_MAJOR}.${KV_MINOR} kernel tree"
HOMEPAGE="https://dev.gentoo.org/~mpagano/genpatches"
SRC_URI="${KERNEL_URI} ${GENPATCHES_URI} ${ARCH_URI}"
KEYWORDS="~riscv"
IUSE="experimental"

PATCHES=(
	# bianbu 6.6 patches git@gitee.com:bianbu-linux/linux-6.6.git
	${FILESDIR}/6.6-spacemitk1-patches/
	# extra fixes
    ${FILESDIR}/add_k1x-thermal_gpl.patch
    ${FILESDIR}/add_license_vcam_dbg.patch
    ${FILESDIR}/fix-spacemit_edp_of_match.patch
    ${FILESDIR}/fix-phy-k1x-ci-otg.patch
    ${FILESDIR}/fix-flexcan-core.patch
    ${FILESDIR}/add-jpu_export.patch
    ${FILESDIR}/add-jpu-dmabuf.patch
    ${FILESDIR}/fix-v2d-dmabuf.patch
    ${FILESDIR}/add-spacemit-pwrreq-gpl-export.patch
    ${FILESDIR}/add-sched-core-gpl-export.patch
    ${FILESDIR}/add-dwc3-spacemit-gpl-export.patch
    ${FILESDIR}/add_ecdev_offer_gpl_export.patch
    ${FILESDIR}/fix-task_stack.patch

)

src_prepare() {
	default
}

pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
