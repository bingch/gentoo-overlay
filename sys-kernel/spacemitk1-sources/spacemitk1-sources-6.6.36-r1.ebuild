# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
ETYPE="sources"
SLOT=0

DESCRIPTION="Full sources for the Linux kernel with spacemit k1 patches"
HOMEPAGE="https://gitlab.com/bingch/linux-6.6"
SRC_URI="https://gitlab.com/bingch/linux-6.6/-/archive/${PV}/linux-6.6-${PV}.tar.bz2"

KEYWORDS="~riscv"

PATCHES=(
    ${FILESDIR}/add_k1x-thermal.patch
    ${FILESDIR}/add_license_vcam_dbg.patch
    ${FILESDIR}/fix-spacemit_edp_of_match.patch
    ${FILESDIR}/fix-phy-k1x-ci-otg.patch
    ${FILESDIR}/fix-flexcan-core.patch
    ${FILESDIR}/add-jpu_export.patch
    ${FILESDIR}/add-jpu-dmabuf.patch
    ${FILESDIR}/fix-v2d-dmabuf.patch
    ${FILESDIR}/add-spacemit-pwrreq-gpl-export.patch
    ${FILESDIR}/add-sched-core-gpl-export.patch
    ${FILESDIR}/add-dwc3-spacemit-gpl-export.patch
    ${FILESDIR}/add_ecdev_offer_gpl_export.patch
)

S=${WORKDIR}/linux-6.6-${PV}/
