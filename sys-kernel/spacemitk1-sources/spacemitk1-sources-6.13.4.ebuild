# Copyright 1999-2025 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
ETYPE="sources"
K_WANT_GENPATCHES="base extras experimental"
K_GENPATCHES_VER="6"

inherit check-reqs kernel-2
detect_version
detect_arch

DESCRIPTION="Full sources including the Gentoo/Spacemit patchset for the ${KV_MAJOR}.${KV_MINOR} kernel tree"
HOMEPAGE="https://dev.gentoo.org/~mpagano/genpatches"
SRC_URI="${KERNEL_URI} ${GENPATCHES_URI} ${ARCH_URI}"
#SRC_URI="https://mirror.serverion.com/irradium/images/milk_v_jupiter/kernel/kernel-k1%236.13.1-1.pkg.tar.gz"
#SRC_URI="https://github.com/jmontleon/linux-bianbu"
#SRC_URI="https://github.com/jmontleon/linux-bianbu-rpm"
KEYWORDS="~riscv"
IUSE="experimental"

PATCHES=(
	# spacemit-k1 patches https://github.com/spacemit-com/linux-k1x/commits/k1/
    ${FILESDIR}/6.13-spacemit-patches/
	# extra patches
    ${FILESDIR}/fix-phy-k1x-ci-otg.patch
    ${FILESDIR}/fix-drm_nulldisp_drv.patch
   # ${FILESDIR}/fix-spacemit_edp_of_match.patch
   # ${FILESDIR}/fix-svivi.patch
    ${FILESDIR}/fix-jpu-dmabuf.patch
    ${FILESDIR}/fix-jpu_export.patch
    ${FILESDIR}/fix-cam_cpp-dmabuf.patch
    ${FILESDIR}/fix-vi-subdev.patch
    ${FILESDIR}/fix-flexcan-core.patch
    ${FILESDIR}/fix-power_supply_class.patch
   # ${FILESDIR}/fix-k1x_rproc.patch
    ${FILESDIR}/fix-ir-spacemit-dup.patch
   # ${FILESDIR}/add_license_vcam_dbg.patch
    ${FILESDIR}/add-spacemit-pwrreq-gpl-export.patch
    ${FILESDIR}/add-sched-syscalls-gpl-export.patch
   # ${FILESDIR}/add_ecdev_offer_gpl_export.patch
   ${FILESDIR}/add-dwc3-spacemit-gpl-export.patch
   # #${FILESDIR}/add-k1x-dma-range-gpl.patch
    ${FILESDIR}/add-spacemit_dpu-gpl-export.patch
    ${FILESDIR}/add-spacemit_wlan_set_power.patch
    ${FILESDIR}/add-k1x-combphy.patch
    #${FILESDIR}/add-spacemit-cpufreq-gpl-license.patch

    ${FILESDIR}/musebook-disable-debug-load-rtw89.patch
   # #${FILESDIR}/readd-spacemit_get_gpio_irq.patch
)

pkg_pretend() {
	CHECKREQS_DISK_BUILD="4G"
	check-reqs_pkg_pretend
}

src_prepare() {
	kernel-2_src_prepare
	rm "${S}/tools/testing/selftests/tc-testing/action-ebpf"
	default
}

pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
	einfo "k1 uboot need itb format kernel, run:"
	einfo "cat arch/riscv/generic/Image.its.S > arch/riscv/boot/Image.its.S"
	einfo 'riscv64-unknown-linux-gnu-gcc -E -P -C -o arch/riscv/boot/Image.gz.its arch/riscv/boot/Image.its.S -DKERNEL_NAME="\"Linux 6.13.0\"" -DIMAGE_COMPRESSION="\"gzip\"" -DIMAGE_CHECK_ALGORITHM="\"crc32\"" -DIMAGE_BINARY="\"Image.gz\"" -DIMAGE_LOAD_ADDRESS=0x200000 -DIMAGE_ENTRY_ADDRESS=0x200000 -DADDR_BITS=64 -DADDR_CELLS=2'
	einfo 'env PATH="./scripts/dtc:/usr/local/sbin:/usr/local/bin:/usr/bin:/opt/bin:/usr/lib/llvm/19/bin:/usr/lib/llvm/18/bin" /bin/sh ./scripts/mkuboot.sh -D "-I dts -O dtb -p 500 --include ./arch/riscv --warning no-unit_address_vs_reg" -f arch/riscv/boot/Image.gz.its arch/riscv/boot/Image.gz.itb'
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
