# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"

SLOT="0"

inherit kernel-2 git-r3
detect_version

KEYWORDS="~riscv"

DEPEND="${RDEPEND}
	>=sys-devel/patch-2.7.5"

DESCRIPTION="Full sources for the Linux kernel for spacemit k1"

EGIT_REPO_URI="https://gitee.com/bianbu-linux/linux-6.1.git"
EGIT_COMMIT="5ad331c2c1c321cdb6c88c6b48d99867eacbefd9"

#SRC_URI="https://github.com/BPI-SINOVOIP/pi-linux/archive/refs/heads/linux-6.1.15-k1.zip"

#S="${WORKDIR}/linux-6.1.15-k1"
PATCHES=(
    ${FILESDIR}/add-cam_plat-export.patch
    ${FILESDIR}/add-dwc3-spacemit-gpl-export.patch
    ${FILESDIR}/add-sched-core-gpl-export.patch
    ${FILESDIR}/add-spacemit-pwrreq-gpl-export.patch

    ${FILESDIR}/fix-cam_cpp-dmabuf.patch
    ${FILESDIR}/fix-jpu-dmabuf.patch
    ${FILESDIR}/fix-k1x_isp_drv-dmabuf.patch
    ${FILESDIR}/fix-mvx-dmabuf.patch
    ${FILESDIR}/fix-spacemit_edp_of_match.patch
    ${FILESDIR}/fix-v2d-dmabuf.patch
)


src_unpack() {
	default
}


src_prepare() {
	default
	eapply_user
}

pkg_postinst() {
	kernel-2_pkg_postinst
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
