#!/bin/bash
systemd-hwdb update
udevadm trigger -v -p DEVNAME=/dev/iio:device0
sleep 1
systemctl restart iio-sensor-proxy.service
sleep 1
gdbus introspect --system --dest net.hadess.SensorProxy --object-path /net/hadess/SensorProxy
