# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
HOMEPAGE="https://chromium.googlesource.com/chromiumos/third_party/kernel"

inherit git-r3
KEYWORDS="~arm64"
SLOT="0"

EGIT_REPO_URI="https://chromium.googlesource.com/chromiumos/third_party/kernel"
if [[ ${PV} != 9999 ]]; then
	EGIT_BRANCH="stabilize-$(ver_cut 1).B-chromeos-$(ver_cut 2-)"
else
    KEYWORDS=""
	EGIT_BRANCH="chromeos-5.15"
fi

DESCRIPTION="Linux kernel with Chromium OS branches,patches & config for chromebook"

RDEPEND=""
DEPEND="${RDEPEND}
	>=sys-devel/patch-2.7.5"

PATCHES=(
	${FILESDIR}/Multigenerational-LRU-Framework-5.15.patch
)

src_unpack() {
	# Revert "CHROMIUM: ASoC: max98357a: Add mixer control to mute/unmute speaker"
	git revert 3d026a8a590f9fb657e8aed00bb76dc1e0e37c08
}
pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
	einfo "To build the kernel use the following command:"
	einfo "make Image Image.gz modules"
	einfo "make DTC_FLAGS="-@" dtbs"
	einfo "make install; make modules_intall; make dtbs_install"
	einfo "If you use kernel config coming with this ebuild, don't forget to also copy/edit fs-display.conf to /etc/dracut.conf.d/"
	einfo "to make sure proper kernel modules are loaded into initramfs"
	einfo "if you want to cross compile arm64 kernel on amd64 host, follow the https://wiki.gentoo.org/wiki/Cross_build_environment"
	einfo "to setup cross toolchain environment, then create a xmake wrapper like the following, and replace make with xmake in above commands"
	einfo "#!/bin/sh"
	einfo "exec make ARCH='arm64' CROSS_COMPILE='aarch64-unknown-linux-gnu-' INSTALL_MOD_PATH='${SYSROOT}' '$@'"
}
