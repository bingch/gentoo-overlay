# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"
inherit kernel-2
detect_version

KEYWORDS="~riscv"

DEPEND="${RDEPEND}
	>=sys-devel/patch-2.7.5"

DESCRIPTION="Full sources for the Linux kernel for VisionFive 2 with Fishwaldo star64 patches"

VF2_TAG="JH7110_VF2_6.6_v5.12.0"
SRC_URI="https://github.com/starfive-tech/linux/archive/refs/tags/${VF2_TAG}.zip -> linux-${VF2_TAG}.zip"

S="${WORKDIR}/linux-${VF2_TAG}"

PATCHES=(
    # patches from https://github.com/Icenowy/linux/commits/star64-6.1/
    ${FILESDIR}/6.6/allow-ecr6600u-as-module.patch
	# verisilicon fix
	${FILESDIR}/6.6/hack-gem-mapping.patch
	${FILESDIR}/6.6/finally-fix-the-cursor-position.patch
	${FILESDIR}/6.6/bias-fb-address-for-dual-head-offset.patch
    ${FILESDIR}/6.6/add-format_mod_supported.patch

    # patches from https://github.com/Fishwaldo/Star64_linux/commits/Pine64_6.6/
	# #${FILESDIR}/6.6/002-init-star64-config-fix-usb-overcurrent-protection.patch
	#${FILESDIR}/6.6/003-compile-gpu-as-module.patch
	#${FILESDIR}/6.6/004-fix-rtc.patch
	#${FILESDIR}/6.6/005-fix-shutdown-with.patch
	#${FILESDIR}/6.6/006-fix-compile-warning.patch
    ${FILESDIR}/6.6/fix-overcurrent-pin.patch
	${FILESDIR}/6.6/add-rtl8852bu-wifi-driver.patch

	#${FILESDIR}/fix-ecrnx-null.patch
	# pinetab v patches
	${FILESDIR}/6.6/add-pinetab-v-dts-6.6-1.patch
	#${FILESDIR}/6.6/fix-pinetab-8g-mem.patch
	${FILESDIR}/6.6/add-pinetab-v-panel-6.6.patch
	# panel driver
	${FILESDIR}/PATCH-v4-1-4-dt-bindings-display-panel-Add-BOE-TH101MB31IG002-28A-panel.patch
	${FILESDIR}/PATCH-v4-2-4-drm-panel-Add-driver-for-BOE-TH101MB31IG002-28A-panel.patch
	${FILESDIR}/PATCH-v4-3-4-dt-bindings-arm64-rockchip-Add-Pine64-PineTab2.patch
	${FILESDIR}/fix-panel-starfive-jadard-iio_st_sensor-ns-import.patch
	#
	${FILESDIR}/0003-add-SC8989-Battery-Charger-and-Charger-Class-6.6.patch
	${FILESDIR}/0043-add-sc89890-support-to-bq25890-driver-6.6.patch
	${FILESDIR}/add-gc02m2-ov5640.patch
	${FILESDIR}/fix-stfcass-dma-buf.patch
	${FILESDIR}/6.6/add-wm8960-license-6.6.patch
	${FILESDIR}/fix-gc02m02-kconfig.patch
	${FILESDIR}/fix-gc02m02_remove-type.patch
)

src_unpack() {
	default
}

src_prepare() {
	default
	eapply_user
}

pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
	einfo "To build the kernel use the following command:"
	einfo "make Image Image.gz modules"
	einfo "make DTC_FLAGS="-@" dtbs"
	einfo "make install; make modules_intall; make dtbs_install"
	einfo "If you use kernel config coming with this ebuild, don't forget to also copy dracut-pp.conf to /etc/dracut.conf.d/"
	einfo "to make sure proper kernel modules are loaded into initramfs"
	einfo "if you want to cross compile pinephone kernel on amd64 host, follow the https://wiki.gentoo.org/wiki/Cross_build_environment"
	einfo "to setup cross toolchain environment, then create a xmake wrapper like the following, and replace make with xmake in above commands"
	einfo "#!/bin/sh"
	einfo "exec make ARCH='riscv' CROSS_COMPILE='riscv64-unknown-linux-gnu-' INSTALL_MOD_PATH='${SYSROOT}' '$@'"
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
