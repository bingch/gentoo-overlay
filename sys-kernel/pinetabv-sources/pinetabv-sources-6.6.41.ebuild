# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"
inherit kernel-2
detect_version

KEYWORDS="~riscv"

DEPEND="${RDEPEND}
	>=sys-devel/patch-2.7.5"

DESCRIPTION="Full sources for the Linux kernel for pinetab v from Icenowy repo"
# https://github.com/Icenowy/linux/commit/5dfb7441bdbbfa20b590db16c93ea0af99bd98d7
SRC_URI="https://github.com/Icenowy/linux/archive/refs/heads/star64-6.6.zip"

PATCHES=(
	${FILESDIR}/6.6/0002-media-i2c-Add-gc02m2-support.patch
	${FILESDIR}/fix-gc02m02-kconfig.patch
	${FILESDIR}/fix-gc02m02_remove-type.patch
	${FILESDIR}/fix-stfcass-dma-buf.patch
	${FILESDIR}/6.6/add-wm8960-license-6.6.patch
	${FILESDIR}/6.6/ecrnx-change-NULL-to-0.patch
	${FILESDIR}/6.6/fix-pinetab-8g-mem-dtsi.patch
	${FILESDIR}/6.6/fix-verisilicon.patch
	${FILESDIR}/6.6/patch-6.6.20-41.patch
)

S="${WORKDIR}/linux-star64-6.6"

src_unpack() {
	default
}

src_prepare() {
	default
	eapply_user
}

pkg_postinst() {
	kernel-2_pkg_postinst
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
