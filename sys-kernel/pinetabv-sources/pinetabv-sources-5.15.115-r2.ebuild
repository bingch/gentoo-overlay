# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"
inherit kernel-2
detect_version

KEYWORDS="~riscv"

DEPEND="${RDEPEND}
	>=sys-devel/patch-2.7.5"

DESCRIPTION="Full sources for the Linux kernel for pinetab v from Fishwaldo repo"

SRC_URI="https://github.com/Fishwaldo/Star64_linux/archive/refs/heads/Star64_devel.zip"

PATCHES=(
    ${FILESDIR}/0001-add-Panel-Driver.patch
	${FILESDIR}/0002-add-es8316-codec.patch
	${FILESDIR}/0003-add-SC8989-Battery-Charger-and-Charger-Class.patch
	${FILESDIR}/0004-Add-update-gc02m2-and-ov5640-camera-support-and-v4l2.patch
	${FILESDIR}/0005-add-pinetabv-dts-and-defconfig.patch
	${FILESDIR}/0006-update-pvr-img-to-1.19.patch
	${FILESDIR}/0007-fix-pwm-bl-off.patch
)

S="${WORKDIR}/Star64_linux-Star64_devel"

src_unpack() {
	default
}

src_prepare() {
	default
	eapply_user
}

pkg_postinst() {
	kernel-2_pkg_postinst
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
