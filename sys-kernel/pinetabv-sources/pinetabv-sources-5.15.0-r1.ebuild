# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"
inherit kernel-2
detect_version

KEYWORDS="~riscv"

DEPEND="${RDEPEND}
	>=sys-devel/patch-2.7.5"

DESCRIPTION="Full sources for the Linux kernel for VisionFive 2"

VF2_TAG="VF2_v3.1.5"
SRC_URI="https://github.com/starfive-tech/linux/archive/refs/tags/${VF2_TAG}.zip -> linux-${VF2_TAG}.zip"

S="${WORKDIR}/linux-${VF2_TAG}"

PATCHES=(
    ${FILESDIR}/0001-staging-Add-rtl8852bu-module.patch
	${FILESDIR}/0002-media-i2c-Add-gc02m2-support.patch
	${FILESDIR}/0003-media-i2c-gc02m2-Resize-resolution-1600x1200-to-1280.patch
	${FILESDIR}/0004-media-starfive-change-swap-dlane0-dlane1.patch
	${FILESDIR}/0005-riscv-dts-starfive-jh7110-enable-gc02m2.patch
	${FILESDIR}/0006-media-i2c-gc02m2-Do-not-change-gpio.patch
)

src_unpack() {
	default
}

src_prepare() {
	default
	eapply_user
}

pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
	einfo "To build the kernel use the following command:"
	einfo "make Image Image.gz modules"
	einfo "make DTC_FLAGS="-@" dtbs"
	einfo "make install; make modules_intall; make dtbs_install"
	einfo "If you use kernel config coming with this ebuild, don't forget to also copy dracut-pp.conf to /etc/dracut.conf.d/"
	einfo "to make sure proper kernel modules are loaded into initramfs"
	einfo "if you want to cross compile pinephone kernel on amd64 host, follow the https://wiki.gentoo.org/wiki/Cross_build_environment"
	einfo "to setup cross toolchain environment, then create a xmake wrapper like the following, and replace make with xmake in above commands"
	einfo "#!/bin/sh"
	einfo "exec make ARCH='riscv' CROSS_COMPILE='riscv64-unknown-linux-gnu-' INSTALL_MOD_PATH='${SYSROOT}' '$@'"
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
