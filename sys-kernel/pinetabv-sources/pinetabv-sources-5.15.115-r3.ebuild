# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"
inherit kernel-2
detect_version

KEYWORDS="~riscv"

DEPEND="${RDEPEND}
	>=sys-devel/patch-2.7.5"

DESCRIPTION="Full sources for the Linux kernel for pinetab v from Fishwaldo repo"

SRC_URI="https://github.com/Fishwaldo/Star64_linux/archive/refs/heads/Star64_devel.zip"

PATCHES=(
	#${FILESDIR}/0002-add-es8316-codec.patch
	#${FILESDIR}/0003-add-SC8989-Battery-Charger-and-Charger-Class.patch
	#${FILESDIR}/0004-Add-update-gc02m2-and-ov5640-camera-support-and-v4l2.patch
	#${FILESDIR}/0005-add-pinetabv-dts-and-defconfig.patch
	${FILESDIR}/0006-update-pvr-img-to-1.19.patch
	${FILESDIR}/0007-fix-pwm-bl-off.patch
    ${FILESDIR}/0001-add-Panel-Driver.patch
	${FILESDIR}/0002-power-bq25890-add-return-values-to-error-messages.patch
	${FILESDIR}/0003-power-supply-bq25890-Fix-race-causing-oops-at-boot.patch
	${FILESDIR}/0004-power-supply-bq25890-Fix-initial-setting-of-the-F_CO.patch
	${FILESDIR}/0005-power-bq25890-add-POWER_SUPPLY_PROP_TEMP.patch
	${FILESDIR}/0006-power-supply-bq25890-Rename-IILIM-field-to-IINLIM.patch
	${FILESDIR}/0007-power-supply-bq25890-Reduce-reported-CONSTANT_CHARGE.patch
	${FILESDIR}/0008-power-supply-bq25890-Add-a-bq25890_rw_init_data-help.patch
    ${FILESDIR}/0009-power-supply-bq25890-Add-support-to-skip-reset-at-pr.patch
    ${FILESDIR}/0010-power-supply-bq25890-Add-support-to-read-back-the-se.patch
    ${FILESDIR}/0011-power-supply-bq25890-Enable-charging-on-boards-where.patch
    ${FILESDIR}/0012-power-supply-bq25890-Drop-dev-platform_data-NULL-che.patch
    ${FILESDIR}/0013-power-supply-bq25890-Add-bq25890_set_otg_cfg-helper.patch
    ${FILESDIR}/0014-power-supply-bq25890-Add-support-for-registering-the.patch
    ${FILESDIR}/0015-power-supply-bq25890-On-the-bq25892-set-the-IINLIM-b.patch
    ${FILESDIR}/0016-power-supply-bq25890-Support-higher-charging-voltage.patch
    ${FILESDIR}/0017-power-supply-bq25890-Use-the-devm_regmap_field_bulk_.patch
    ${FILESDIR}/0018-power-supply-bq25890-Disable-PUMPX_EN-on-errors.patch
    ${FILESDIR}/0019-power-supply-bq25890-Add-support-for-setting-IINLIM.patch
    ${FILESDIR}/0020-Revert-power-supply-bq25890-Add-support-for-register.patch
    ${FILESDIR}/0021-power-supply-bq25890-Add-support-for-registering-the.patch
    ${FILESDIR}/0022-power-supply-bq25890-Fix-enum-conversion-in-bq25890_.patch
    ${FILESDIR}/0023-power-supply-bq25890-Document-POWER_SUPPLY_PROP_CURR.patch
	${FILESDIR}/0024-power-supply-bq25890-Clean-up-POWER_SUPPLY_PROP_CONS.patch
    ${FILESDIR}/0025-power-supply-bq25890-Clean-up-POWER_SUPPLY_PROP_CONS.patch
    ${FILESDIR}/0026-power-supply-bq25890-Add-support-for-setting-user-ch.patch
    ${FILESDIR}/0027-power-supply-bq25890-Factor-out-regulator-registrati.patch
    ${FILESDIR}/0028-power-supply-bq25890-Add-get_voltage-support-to-Vbus.patch
    ${FILESDIR}/0029-power-supply-bq25890-Add-Vsys-regulator.patch
    ${FILESDIR}/0030-power-supply-bq25890-Only-use-pdata-regulator_init_d.patch
    ${FILESDIR}/0031-power-supply-bq25890-Ensure-pump_express_work-is-can.patch
    ${FILESDIR}/0032-power-supply-bq25890-Fix-usb-notifier-probe-and-remo.patch
    ${FILESDIR}/0033-power-supply-bq25890-Factor-out-chip-state-update.patch
    ${FILESDIR}/0034-power-supply-bq25890-Add-HiZ-mode-support.patch
    ${FILESDIR}/0035-power-supply-bq25890-Fix-setting-of-F_CONV_RATE-rate.patch
    ${FILESDIR}/0036-power-supply-bq25890-Always-take-HiZ-mode-into-accou.patch
    ${FILESDIR}/0037-power-supply-bq25890-Support-boards-with-more-then-o.patch
    ${FILESDIR}/0038-power-supply-bq25890-Add-support-for-having-a-second.patch
    ${FILESDIR}/0039-power-supply-bq25890-Add-new-linux-iinlim-percentage.patch
    ${FILESDIR}/0040-power-supply-bq25890_charger-mark-OF-related-data-as.patch
    ${FILESDIR}/0041-power-supply-bq25890-Fix-external_power_changed-race.patch
    ${FILESDIR}/0042-power-supply-bq25890-Call-power_supply_changed-after.patch
    ${FILESDIR}/0043-add-sc89890-support-to-bq25890-driver.patch
    ${FILESDIR}/0044-Add-update-gc02m2-and-ov5640-camera-support-and-v4l2.patch
    ${FILESDIR}/0045-ASoC-es8316-add-support-for-ESSX8336-ACPI-_HID.patch
    ${FILESDIR}/0046-ASoC-es8316-Use-modern-ASoC-DAI-format-terminology.patch
    ${FILESDIR}/0047-ASoC-es-Remove-now-redundant-non_legacy_dai_naming-f.patch
    ${FILESDIR}/0048-ASoC-codes-Add-support-for-ES8316-producer-mode.patch
    ${FILESDIR}/0049-ASoC-codecs-add-suspend-and-resume-for-ES8316.patch
    ${FILESDIR}/0050-ASoC-es8316-fix-register-sync-error-in-suspend-resum.patch
    ${FILESDIR}/0051-ASoC-es8316-Don-t-use-ranges-based-register-lookup-f.patch
    ${FILESDIR}/0052-ASoC-es8316-Increment-max-value-for-ALC-Capture-Targ.patch
    ${FILESDIR}/0053-ASoC-es8316-Do-not-set-rate-constraints-for-unsuppor.patch
    ${FILESDIR}/0054-ASoc-codecs-ES8316-Fix-DMIC-config.patch
    ${FILESDIR}/0055-add-pinetabv-dts-and-defconfig.patch
)

S="${WORKDIR}/Star64_linux-Star64_devel"

src_unpack() {
	default
}

src_prepare() {
	default
	eapply_user
}

pkg_postinst() {
	kernel-2_pkg_postinst
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
