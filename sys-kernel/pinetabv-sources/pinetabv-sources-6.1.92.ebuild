# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"
inherit kernel-2
detect_version

KEYWORDS="~riscv"

DEPEND="${RDEPEND}
	>=sys-devel/patch-2.7.5"

DESCRIPTION="Full sources for the Linux kernel for pinetab v from MichaIng repo"
# https://github.com/MichaIng/linux/commit/aba7fbc1e1a766329fcd55282c2bdd554c039c83
SRC_URI="https://github.com/MichaIng/linux/archive/refs/heads/6.1-star64.zip"

PATCHES=(
	${FILESDIR}/add-pinetab-v-dts.patch
	${FILESDIR}/add-pinetab-v-panel.patch
	${FILESDIR}/0003-add-SC8989-Battery-Charger-and-Charger-Class.patch
	${FILESDIR}/fix-bq25890.patch
	${FILESDIR}/0043-add-sc89890-support-to-bq25890-driver-fix.patch
	${FILESDIR}/add-wm8960-license.patch
	${FILESDIR}/fix-gc02m02-kconfig.patch
	${FILESDIR}/fix-gc02m02_remove-type.patch
	${FILESDIR}/fix-starfive-ov5640.patch
	${FILESDIR}/fix-dw-axi-dmac.patch
	${FILESDIR}/change-NULL-to-0.patch 
)

S="${WORKDIR}/linux-6.1-star64"

src_unpack() {
	default
}

src_prepare() {
	default
	eapply_user
}

pkg_postinst() {
	kernel-2_pkg_postinst
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
