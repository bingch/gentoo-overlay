diff --git a/drivers/gpu/drm/panel/Kconfig b/drivers/gpu/drm/panel/Kconfig
index 886edb0..44588a6 100644
--- a/drivers/gpu/drm/panel/Kconfig
+++ b/drivers/gpu/drm/panel/Kconfig
@@ -748,4 +748,14 @@ config DRM_PANEL_XINPENG_XPP055C272
 	  Say Y here if you want to enable support for the Xinpeng
 	  XPP055C272 controller for 720x1280 LCD panels with MIPI/RGB/SPI
 	  system interfaces.
+
+config DRM_PANEL_PINETAB_V
+	tristate "PineTab-V WUXGA DSI panel"
+	depends on OF
+	depends on DRM_MIPI_DSI
+	depends on BACKLIGHT_CLASS_DEVICE
+	help
+	  Say Y here if you want to enable support for the LCD panel in
+	  PineTab-V tablet. The panel comes with 800x1280 LCD.
+
 endmenu
diff --git a/drivers/gpu/drm/panel/Makefile b/drivers/gpu/drm/panel/Makefile
index 0d161de..5cda8fd 100644
--- a/drivers/gpu/drm/panel/Makefile
+++ b/drivers/gpu/drm/panel/Makefile
@@ -76,3 +76,4 @@ obj-$(CONFIG_DRM_PANEL_TRULY_NT35597_WQXGA) += panel-truly-nt35597.o
 obj-$(CONFIG_DRM_PANEL_VISIONOX_RM69299) += panel-visionox-rm69299.o
 obj-$(CONFIG_DRM_PANEL_WIDECHIPS_WS2401) += panel-widechips-ws2401.o
 obj-$(CONFIG_DRM_PANEL_XINPENG_XPP055C272) += panel-xinpeng-xpp055c272.o
+obj-$(CONFIG_DRM_PANEL_PINETAB_V) += panel-pinetab-v.o
diff --git a/drivers/gpu/drm/panel/panel-pinetab-v.c b/drivers/gpu/drm/panel/panel-pinetab-v.c
new file mode 100755
index 0000000..9c84f93
--- /dev/null
+++ b/drivers/gpu/drm/panel/panel-pinetab-v.c
@@ -0,0 +1,502 @@
+// SPDX-License-Identifier: GPL-2.0+
+/*
+ * Copyright (c) 2019 Radxa Limited
+ * Copyright (c) 2022 Edgeble AI Technologies Pvt. Ltd.
+ *
+ * Author:
+ * - Jagan Teki <jagan@amarulasolutions.com>
+ * - Stephen Chen <stephen@radxa.com>
+ */
+
+#include <drm/drm_mipi_dsi.h>
+#include <drm/drm_modes.h>
+#include <drm/drm_panel.h>
+#include <drm/drm_print.h>
+
+#include <linux/gpio/consumer.h>
+#include <linux/delay.h>
+#include <linux/module.h>
+#include <linux/of_device.h>
+#include <linux/regulator/consumer.h>
+
+#include <linux/device.h>
+#include <linux/i2c.h>
+#include <linux/kernel.h>
+#include <linux/of.h>
+#include <linux/of_graph.h>
+#include <drm/drm_crtc.h>
+#include <drm/drm_device.h>
+#include <video/display_timing.h>
+#include <video/videomode.h>
+
+#define DSI_DRIVER_NAME "pinetab-v-panel"
+
+enum cmd_type {
+	CMD_TYPE_DCS,
+	CMD_TYPE_DELAY,
+};
+
+struct jadard_init_cmd {
+	enum cmd_type type;
+	const char *data;
+	size_t len;
+};
+
+#define _INIT_CMD_DCS(...)					\
+	{							\
+		.type	= CMD_TYPE_DCS,				\
+		.data	= (char[]){__VA_ARGS__},		\
+		.len	= sizeof((char[]){__VA_ARGS__})		\
+	}							\
+
+#define _INIT_CMD_DELAY(...)					\
+	{							\
+		.type	= CMD_TYPE_DELAY,			\
+		.data	= (char[]){__VA_ARGS__},		\
+		.len	= sizeof((char[]){__VA_ARGS__})		\
+	}							\
+
+struct jadard_panel_desc {
+	const struct drm_display_mode mode;
+	unsigned int lanes;
+	enum mipi_dsi_pixel_format format;
+	const struct jadard_init_cmd *init_cmds;
+	u32 num_init_cmds;
+};
+
+struct jadard {
+	struct drm_panel panel;
+	struct mipi_dsi_device *dsi;
+	const struct jadard_panel_desc *desc;
+	struct i2c_client *client;
+
+	struct device   *dev;
+
+	struct regulator *vdd;
+	struct regulator *vccio;
+	struct gpio_desc *reset;
+	struct gpio_desc *enable;
+	enum drm_panel_orientation orientation;
+	bool enable_initialized;
+};
+
+static inline struct jadard *panel_to_jadard(struct drm_panel *panel)
+{
+	return container_of(panel, struct jadard, panel);
+}
+
+static int jadard_i2c_write(struct i2c_client *client, u8 reg, u8 val)
+{
+	struct i2c_msg msg;
+	u8 buf[2];
+	int ret;
+
+	buf[0] = reg;
+	buf[1] = val;
+	msg.addr = client->addr;
+	msg.flags = 0;
+	msg.buf = buf;
+	msg.len = 2;
+
+	ret = i2c_transfer(client->adapter, &msg, 1);
+	if (ret >= 0)
+		return 0;
+
+	return ret;
+}
+
+static int jadard_i2c_read(struct i2c_client *client, u8 reg, u8 *val)
+{
+	struct i2c_msg msg[2];
+	u8 buf[2];
+	int ret;
+
+	buf[0] = reg;
+	msg[0].addr = client->addr;
+	msg[0].flags = 0;
+	msg[0].buf = buf;
+	msg[0].len = 1;
+	msg[1].addr = client->addr;
+	msg[1].flags = I2C_M_RD;
+	msg[1].buf = val;
+	msg[1].len = 1;
+	ret = i2c_transfer(client->adapter, msg, 2);
+
+	if (ret >= 0)
+		return 0;
+
+	return ret;
+}
+
+static int jadard_enable(struct drm_panel *panel)
+{
+	struct device *dev = panel->dev;
+	struct jadard *jadard = panel_to_jadard(panel);
+	const struct jadard_panel_desc *desc = jadard->desc;
+	struct mipi_dsi_device *dsi = jadard->dsi;
+	unsigned int i;
+	int err;
+	if (jadard->enable_initialized == true)
+		return 0;
+
+	for (i = 0; i < desc->num_init_cmds; i++) {
+		const struct jadard_init_cmd *cmd = &desc->init_cmds[i];
+
+		switch (cmd->type) {
+		case CMD_TYPE_DELAY:
+			msleep(cmd->data[0]);
+			err = 0;
+			break;
+		case CMD_TYPE_DCS:
+			/*err = mipi_dsi_dcs_write(dsi, cmd->data[0],
+						 cmd->len <= 1 ? NULL : &cmd->data[1],
+						 cmd->len - 1);
+			*/
+			err = mipi_dsi_dcs_write_buffer(dsi, cmd->data, cmd->len);
+			break;
+		default:
+			err = -EINVAL;
+		}
+
+		if (err < 0) {
+			DRM_DEV_ERROR(dev, "failed to write CMD#0x%x\n", cmd->data[0]);
+			return err;
+		}
+
+	}
+
+	err = mipi_dsi_dcs_exit_sleep_mode(dsi);
+	if (err < 0)
+		DRM_DEV_ERROR(dev, "failed to exit sleep mode ret = %d\n", err);
+	msleep(120);
+
+	err =  mipi_dsi_dcs_set_display_on(dsi);
+	if (err < 0)
+		DRM_DEV_ERROR(dev, "failed to set display on ret = %d\n", err);
+	jadard->enable_initialized = true ;
+
+	return 0;
+}
+
+static int jadard_disable(struct drm_panel *panel)
+{
+	struct device *dev = panel->dev;
+	struct jadard *jadard = panel_to_jadard(panel);
+	int ret;
+
+	jadard->enable_initialized = false;
+
+	return 0;
+}
+
+static int jadard_prepare(struct drm_panel *panel)
+{
+	struct device *dev = panel->dev;
+	struct jadard *jadard = panel_to_jadard(panel);
+	const struct jadard_panel_desc *desc = jadard->desc;
+	struct mipi_dsi_device *dsi = jadard->dsi;
+	unsigned int i;
+	int err;
+
+	if (jadard->enable_initialized == true)
+		return 0;
+
+	msleep(250);
+
+	return 0;
+}
+
+static int jadard_unprepare(struct drm_panel *panel)
+{
+	struct jadard *jadard = panel_to_jadard(panel);
+
+	msleep(120);
+	return 0;
+}
+
+static int jadard_get_modes(struct drm_panel *panel,
+			    struct drm_connector *connector)
+{
+	struct jadard *jadard = panel_to_jadard(panel);
+	const struct drm_display_mode *desc_mode = &jadard->desc->mode;
+	struct drm_display_mode *mode;
+
+	mode = drm_mode_duplicate(connector->dev, desc_mode);
+	if (!mode) {
+		DRM_DEV_ERROR(&jadard->dsi->dev, "failed to add mode %ux%ux@%u\n",
+			      desc_mode->hdisplay, desc_mode->vdisplay,
+			      drm_mode_vrefresh(desc_mode));
+		return -ENOMEM;
+	}
+
+	drm_mode_set_name(mode);
+	drm_mode_probed_add(connector, mode);
+
+	connector->display_info.width_mm = mode->width_mm;
+	connector->display_info.height_mm = mode->height_mm;
+
+	drm_connector_set_panel_orientation(connector, jadard->orientation);
+
+	return 1;
+}
+
+static const struct drm_panel_funcs jadard_funcs = {
+	.disable = jadard_disable,
+	.unprepare = jadard_unprepare,
+	.prepare = jadard_prepare,
+	.enable = jadard_enable,
+	.get_modes = jadard_get_modes,
+};
+
+static const struct jadard_init_cmd cz101b4001_init_cmds[] = {
+_INIT_CMD_DCS(0xE0,0xAB,0xBA), 
+_INIT_CMD_DCS(0xE1,0xBA,0xAB), 
+_INIT_CMD_DCS(0xB1,0x10,0x01,0x47,0xFF), 
+_INIT_CMD_DCS(0xB2,0x0C,0x14,0x04,0x50,0x50,0x14), 
+_INIT_CMD_DCS(0xB3,0x56,0x53,0x00), 
+_INIT_CMD_DCS(0xB4,0x33,0x30,0x04), 
+_INIT_CMD_DCS(0xB6,0xB0,0x00,0x00,0x10,0x00,0x10,0x00), 
+_INIT_CMD_DCS(0xB8,0x05,0x12,0x29,0x49,0x48,0x00,0x00), 
+_INIT_CMD_DCS(0xB9,0x7C,0x65,0x55,0x49,0x46,0x36,0x3B,0x24,0x3D,0x3C,0x3D,0x5C,0x4C,0x55,0x47,0x46,0x39,0x26,0x06,0x7C,0x65,0x55,0x49,0x46,0x36,0x3B,0x24,0x3D,0x3C,0x3D,0x5C,0x4C,0x55,0x47,0x46,0x39,0x26,0x06), 
+_INIT_CMD_DCS(0xC0,0xFF,0x87,0x12,0x34,0x44,0x44,0x44,0x44,0x98,0x04,0x98,0x04,0x0F,0x00,0x00,0xC1), 
+_INIT_CMD_DCS(0xC1,0x54,0x94,0x02,0x85,0x9F,0x00,0x7F,0x00,0x54,0x00), 
+_INIT_CMD_DCS(0xC2,0x17,0x09,0x08,0x89,0x08,0x11,0x22,0x20,0x44,0xFF,0x18,0x00), 
+_INIT_CMD_DCS(0xC3,0x86,0x46,0x05,0x05,0x1C,0x1C,0x1D,0x1D,0x02,0x1F,0x1F,0x1E,0x1E,0x0F,0x0F,0x0D,0x0D,0x13,0x13,0x11,0x11,0x00), 
+_INIT_CMD_DCS(0xC4,0x07,0x07,0x04,0x04,0x1C,0x1C,0x1D,0x1D,0x02,0x1F,0x1F,0x1E,0x1E,0x0E,0x0E,0x0C,0x0C,0x12,0x12,0x10,0x10,0x00), 
+_INIT_CMD_DCS(0xC6,0x2A,0x2A), 
+_INIT_CMD_DCS(0xC8,0x21,0x00,0x31,0x42,0x34,0x16), 
+_INIT_CMD_DCS(0xCA,0xCB,0x43), 
+_INIT_CMD_DCS(0xCD,0x0E,0x4B,0x4B,0x20,0x19,0x6B,0x06,0xB3), 
+_INIT_CMD_DCS(0xD2,0xE3,0x2B,0x38,0x00), 
+_INIT_CMD_DCS(0xD4,0x00,0x01,0x00,0x0E,0x04,0x44,0x08,0x10,0x00,0x00,0x00), 
+_INIT_CMD_DCS(0xE6,0x80,0x01,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF), 
+_INIT_CMD_DCS(0xF0,0x12,0x03,0x20,0x00,0xFF), 
+_INIT_CMD_DCS(0xF3,0x00), 
+
+	_INIT_CMD_DELAY(120),
+};
+
+static const struct jadard_panel_desc cz101b4001_desc = {
+	.mode = {
+
+	.clock		= 66000,
+	.hdisplay	= 800,
+	.hsync_start	= 800 + 64,
+	.hsync_end	= 800 + 64 + 16,
+	.htotal		= 800 + 64 + 16 + 64,
+	.vdisplay	= 1280,
+	.vsync_start	= 1280 + 2,
+	.vsync_end	= 1280 + 2 + 4,
+	.vtotal		= 1280 + 2 + 4 + 12,
+	.width_mm	= 135,
+	.height_mm	= 216,
+		.type		= DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED,
+	},
+	.lanes = 4,
+	.format = MIPI_DSI_FMT_RGB888,
+	.init_cmds = cz101b4001_init_cmds,
+	.num_init_cmds = ARRAY_SIZE(cz101b4001_init_cmds),
+};
+
+static int panel_probe(struct i2c_client *client)
+{
+	u8 reg_value = 0;
+	struct jadard *jd_panel;
+	const struct jadard_panel_desc *desc;
+
+	struct device_node *endpoint, *dsi_host_node;
+	struct mipi_dsi_host *host;
+	struct device *dev = &client->dev;
+	int ret = 0;
+	struct mipi_dsi_device_info info = {
+		.type = DSI_DRIVER_NAME,
+		.channel = 3,
+		.node = NULL,
+	};
+
+	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
+		dev_warn(&client->dev,
+			 "I2C adapter doesn't support I2C_FUNC_SMBUS_BYTE\n");
+		return -EIO;
+	}
+
+	jd_panel = devm_kzalloc(&client->dev, sizeof(struct jadard), GFP_KERNEL);
+	if (!jd_panel )
+		return -ENOMEM;
+	desc = of_device_get_match_data(dev);
+
+	jd_panel ->client = client;
+	i2c_set_clientdata(client, jd_panel);
+
+	jd_panel->enable_initialized = false;
+
+	jd_panel->reset = devm_gpiod_get_optional(dev, "reset", GPIOD_OUT_HIGH);
+	if (IS_ERR(jd_panel->reset)) {
+		DRM_DEV_ERROR(dev, "failed to get our reset GPIO\n");
+		//return PTR_ERR(jd_panel->reset);
+	}
+
+	jd_panel->enable = devm_gpiod_get(dev, "enable", GPIOD_OUT_LOW);
+	if (IS_ERR(jd_panel->enable)) {
+		DRM_DEV_ERROR(dev, "failed to get our enable GPIO\n");
+		return PTR_ERR(jd_panel->enable);
+	}
+	gpiod_direction_output(jd_panel->enable, 1);
+
+	endpoint = of_graph_get_next_endpoint(dev->of_node, NULL);
+	if (!endpoint)
+		return -ENODEV;
+
+	dsi_host_node = of_graph_get_remote_port_parent(endpoint);
+	if (!dsi_host_node)
+		goto error;
+
+	host = of_find_mipi_dsi_host_by_node(dsi_host_node);
+	of_node_put(dsi_host_node);
+	if (!host) {
+		of_node_put(endpoint);
+		return -EPROBE_DEFER;
+	}
+
+	drm_panel_init(&jd_panel->panel, dev, &jadard_funcs,
+		       DRM_MODE_CONNECTOR_DSI);
+
+	ret = of_drm_get_panel_orientation(dev->of_node, &jd_panel->orientation);
+	if (ret) {
+		dev_err(dev, "%pOF: failed to get orientation %d\n", dev->of_node, ret);
+		goto error;
+	}
+
+	ret = drm_panel_of_backlight(&jd_panel->panel);
+	if (ret)
+		goto error;
+
+	drm_panel_add(&jd_panel->panel);
+
+	info.node = of_node_get(of_graph_get_remote_port(endpoint));
+	if (!info.node) {
+		ret = -ENODEV;
+		goto error;
+	}
+
+	of_node_put(endpoint);
+	jd_panel->desc = desc;
+
+	jd_panel->dsi = mipi_dsi_device_register_full(host, &info);
+	if (IS_ERR(jd_panel->dsi)) {
+		dev_err(dev, "DSI device registration failed: %ld\n",
+			PTR_ERR(jd_panel->dsi));
+		return PTR_ERR(jd_panel->dsi);
+	}
+
+	mipi_dsi_set_drvdata(jd_panel->dsi, jd_panel);
+
+	return 0;
+error:
+	of_node_put(endpoint);
+	return ret;
+no_panel:
+	mipi_dsi_device_unregister(jd_panel->dsi);
+
+	return -ENODEV;
+
+
+}
+
+static void panel_remove(struct i2c_client *client)
+{
+	struct jadard *jd_panel = i2c_get_clientdata(client);
+
+	mipi_dsi_detach(jd_panel->dsi);
+	drm_panel_remove(&jd_panel->panel);
+	mipi_dsi_device_unregister(jd_panel->dsi);
+}
+
+static const struct i2c_device_id panel_id[] = {
+	{ "pinetab-v-panel", 0 },
+	{ }
+};
+
+static const struct of_device_id panel_dt_ids[] = {
+	{ .compatible = "pine64,pinetab-v-panel", .data = &cz101b4001_desc},
+	{ /* sentinel */ }
+};
+
+static struct i2c_driver panel_driver = {
+	.driver = {
+		.owner	= THIS_MODULE,
+		.name	= "pinetab-v-panel",
+		.of_match_table = panel_dt_ids,
+	},
+	.probe		= panel_probe,
+	.remove		= panel_remove,
+	.id_table	= panel_id,
+};
+
+static int jadard_dsi_probe(struct mipi_dsi_device *dsi)
+{
+	struct device *dev = &dsi->dev;
+	struct jadard *jadard = mipi_dsi_get_drvdata(dsi);
+
+	int ret;
+
+	dsi->mode_flags = MIPI_DSI_MODE_LPM | MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_SYNC_PULSE | MIPI_DSI_CLOCK_NON_CONTINUOUS;
+	dsi->format = MIPI_DSI_FMT_RGB888;
+	dsi->lanes = 4;
+	dsi->channel = 3;
+	dsi->hs_rate = 400000000;
+
+	ret = mipi_dsi_attach(dsi);
+	if (ret < 0) {
+		return ret;
+	}
+
+	return 0;
+}
+
+static void jadard_dsi_remove(struct mipi_dsi_device *dsi)
+{
+	struct jadard *jadard = mipi_dsi_get_drvdata(dsi);
+
+	mipi_dsi_detach(dsi);
+}
+
+static const struct of_device_id jadard_of_match[] = {
+	{ .compatible = "pine64,pinetab-v-panel-dsi", .data = &cz101b4001_desc },
+	{ /* sentinel */ }
+};
+MODULE_DEVICE_TABLE(of, jadard_of_match);
+
+static struct mipi_dsi_driver jadard_mipi_driver = {
+	.probe = jadard_dsi_probe,
+	.remove = jadard_dsi_remove,
+	.driver.name = DSI_DRIVER_NAME,
+};
+//module_mipi_dsi_driver(jadard_driver);
+
+
+static int __init init_panel(void)
+{
+	int err;
+
+	mipi_dsi_driver_register(&jadard_mipi_driver);
+	err = i2c_add_driver(&panel_driver);
+
+	return err;
+
+}
+module_init(init_panel);
+
+static void __exit exit_panel(void)
+{
+	i2c_del_driver(&panel_driver);
+	mipi_dsi_driver_unregister(&jadard_mipi_driver);
+}
+module_exit(exit_panel);
+
+
+MODULE_AUTHOR("Jagan Teki <jagan@edgeble.ai>");
+MODULE_AUTHOR("Stephen Chen <stephen@radxa.com>");
+MODULE_DESCRIPTION("Jadard JD9365DA-H3 WUXGA DSI panel");
+MODULE_LICENSE("GPL");
+
