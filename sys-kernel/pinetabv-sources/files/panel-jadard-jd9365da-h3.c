// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (c) 2019 Radxa Limited
 * Copyright (c) 2022 Edgeble AI Technologies Pvt. Ltd.
 *
 * Author:
 * - Jagan Teki <jagan@amarulasolutions.com>
 * - Stephen Chen <stephen@radxa.com>
 */

#include <drm/drm_mipi_dsi.h>
#include <drm/drm_modes.h>
#include <drm/drm_panel.h>
#include <drm/drm_print.h>

#include <linux/gpio/consumer.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/regulator/consumer.h>

#define JD9365DA_INIT_CMD_LEN		2

struct jadard_init_cmd {
	u8 data[JD9365DA_INIT_CMD_LEN];
};

struct jadard_panel_desc {
	const struct drm_display_mode mode;
	unsigned int lanes;
	enum mipi_dsi_pixel_format format;
	const struct jadard_init_cmd *init_cmds;
	u32 num_init_cmds;
};

struct jadard {
	struct drm_panel panel;
	struct mipi_dsi_device *dsi;
	const struct jadard_panel_desc *desc;

	struct regulator *vdd;
	struct regulator *vccio;
	struct gpio_desc *reset;
};

static inline struct jadard *panel_to_jadard(struct drm_panel *panel)
{
	return container_of(panel, struct jadard, panel);
}

static int jadard_enable(struct drm_panel *panel)
{
	struct device *dev = panel->dev;
	struct jadard *jadard = panel_to_jadard(panel);
	const struct jadard_panel_desc *desc = jadard->desc;
	struct mipi_dsi_device *dsi = jadard->dsi;
	unsigned int i;
	int err;

	msleep(10);

	for (i = 0; i < desc->num_init_cmds; i++) {
		const struct jadard_init_cmd *cmd = &desc->init_cmds[i];

		err = mipi_dsi_dcs_write_buffer(dsi, cmd->data, JD9365DA_INIT_CMD_LEN);
		if (err < 0)
			return err;
	}

	msleep(120);

	err = mipi_dsi_dcs_exit_sleep_mode(dsi);
	if (err < 0)
		DRM_DEV_ERROR(dev, "failed to exit sleep mode ret = %d\n", err);

	err =  mipi_dsi_dcs_set_display_on(dsi);
	if (err < 0)
		DRM_DEV_ERROR(dev, "failed to set display on ret = %d\n", err);

	return 0;
}

static int jadard_disable(struct drm_panel *panel)
{
	struct device *dev = panel->dev;
	struct jadard *jadard = panel_to_jadard(panel);
	int ret;

	ret = mipi_dsi_dcs_set_display_off(jadard->dsi);
	if (ret < 0)
		DRM_DEV_ERROR(dev, "failed to set display off: %d\n", ret);

	ret = mipi_dsi_dcs_enter_sleep_mode(jadard->dsi);
	if (ret < 0)
		DRM_DEV_ERROR(dev, "failed to enter sleep mode: %d\n", ret);

	return 0;
}

static int jadard_prepare(struct drm_panel *panel)
{
	struct jadard *jadard = panel_to_jadard(panel);
	int ret;

	ret = regulator_enable(jadard->vccio);
	if (ret)
		return ret;

	ret = regulator_enable(jadard->vdd);
	if (ret)
		return ret;

	gpiod_set_value(jadard->reset, 1);
	msleep(5);

	gpiod_set_value(jadard->reset, 0);
	msleep(10);

	gpiod_set_value(jadard->reset, 1);
	msleep(120);

	return 0;
}

static int jadard_unprepare(struct drm_panel *panel)
{
	struct jadard *jadard = panel_to_jadard(panel);

	gpiod_set_value(jadard->reset, 1);
	msleep(120);

	regulator_disable(jadard->vdd);
	regulator_disable(jadard->vccio);

	return 0;
}

static int jadard_get_modes(struct drm_panel *panel,
			    struct drm_connector *connector)
{
	struct jadard *jadard = panel_to_jadard(panel);
	const struct drm_display_mode *desc_mode = &jadard->desc->mode;
	struct drm_display_mode *mode;

	mode = drm_mode_duplicate(connector->dev, desc_mode);
	if (!mode) {
		DRM_DEV_ERROR(&jadard->dsi->dev, "failed to add mode %ux%ux@%u\n",
			      desc_mode->hdisplay, desc_mode->vdisplay,
			      drm_mode_vrefresh(desc_mode));
		return -ENOMEM;
	}

	drm_mode_set_name(mode);
	drm_mode_probed_add(connector, mode);

	connector->display_info.width_mm = mode->width_mm;
	connector->display_info.height_mm = mode->height_mm;

	return 1;
}

static const struct drm_panel_funcs jadard_funcs = {
	.disable = jadard_disable,
	.unprepare = jadard_unprepare,
	.prepare = jadard_prepare,
	.enable = jadard_enable,
	.get_modes = jadard_get_modes,
};

static const struct jadard_init_cmd radxa_display_8hd_ad002_init_cmds[] = {
	{ .data = { 0xE0, 0xAB } },
	{ .data = { 0xBA, 0xE1 } },
	{ .data = { 0xBA, 0xAB } },
	{ .data = { 0xB1, 0x10 } },
	{ .data = { 0x01, 0x47 } },
	{ .data = { 0xFF, 0xB2 } },
	{ .data = { 0x0C, 0x14 } },
	{ .data = { 0x04, 0x50 } },
	{ .data = { 0x50, 0x14 } },
	{ .data = { 0xB3, 0x56 } },
	{ .data = { 0x53, 0x00 } },
	{ .data = { 0xB4, 0x33 } },
	{ .data = { 0x30, 0x04 } },
	{ .data = { 0xB6, 0xB0 } },
	{ .data = { 0x00, 0x00 } },
	{ .data = { 0x10, 0x00 } },
	{ .data = { 0x10, 0x00 } },
	{ .data = { 0xB8, 0x05 } },
	{ .data = { 0x12, 0x29 } },
	{ .data = { 0x49, 0x48 } },
	{ .data = { 0x00, 0x00 } },
	{ .data = { 0xB9, 0x7C } },
	{ .data = { 0x65, 0x55 } },
	{ .data = { 0x49, 0x46 } },
	{ .data = { 0x36, 0x3B } },
	{ .data = { 0x24, 0x3D } },
	{ .data = { 0x3C, 0x3D } },
	{ .data = { 0x5C, 0x4C } },
        { .data = { 0x55, 0x47 } },
        { .data = { 0x46, 0x39 } },
	{ .data = { 0x26, 0x06 } },
	{ .data = { 0x7C, 0x65 } },
	{ .data = { 0x55, 0x49 } },
	{ .data = { 0x46, 0x36 } },
	{ .data = { 0x3B, 0x24 } },
	{ .data = { 0x3D, 0x3C } },
	{ .data = { 0x3D, 0x5C } },
	{ .data = { 0x4C, 0x55 } },
	{ .data = { 0x47, 0x46 } },
	{ .data = { 0x39, 0x26 } },
	{ .data = { 0x06, 0xC0 } },
	{ .data = { 0xFF, 0x87 } },
	{ .data = { 0x12, 0x34 } },
	{ .data = { 0x44, 0x44 } },
	{ .data = { 0x44, 0x44 } },
	{ .data = { 0x98, 0x04 } },
	{ .data = { 0x98, 0x04 } },
	{ .data = { 0x0F, 0x00 } },
	{ .data = { 0x00, 0xC1 } },
	{ .data = { 0xC1, 0x54 } },
	{ .data = { 0x94, 0x02 } },
	{ .data = { 0x85, 0x9F } },
	{ .data = { 0x00, 0x7F } },
	{ .data = { 0x00, 0x54 } },
	{ .data = { 0x00, 0xC2 } },
	{ .data = { 0x17, 0x09 } },
	{ .data = { 0x08, 0x89 } },
	{ .data = { 0x08, 0x11 } },
	{ .data = { 0x22, 0x20 } },
	{ .data = { 0x44, 0xFF } },
	{ .data = { 0x18, 0x00 } },
	{ .data = { 0xC3, 0x86 } },
	{ .data = { 0x46, 0x05 } },
	{ .data = { 0x05, 0x1C } },
	{ .data = { 0x1C, 0x1D } },
	{ .data = { 0x1D, 0x02 } },
	{ .data = { 0x1F, 0x1F } },
	{ .data = { 0x1E, 0x1E } },
	{ .data = { 0x0F, 0x0F } },
	{ .data = { 0x0D, 0x0D } },
	{ .data = { 0x13, 0x13 } },
	{ .data = { 0x11, 0x11 } },
	{ .data = { 0x00, 0xC4 } },
	{ .data = { 0x07, 0x07 } },
	{ .data = { 0x04, 0x04 } },
	{ .data = { 0x1C, 0x1C } },
	{ .data = { 0x1D, 0x1D } },
	{ .data = { 0x02, 0x1F } },
	{ .data = { 0x1F, 0x1E } },
	{ .data = { 0x1E, 0x0E } },
	{ .data = { 0x0E, 0x0C } },
	{ .data = { 0x0C, 0x12 } },
	{ .data = { 0x12, 0x10 } },
	{ .data = { 0x10, 0x00 } },
	{ .data = { 0xC6, 0x2A } },
	{ .data = { 0x2A, 0xC8 } },
	{ .data = { 0x21, 0x00 } },
	{ .data = { 0x31, 0x42 } },
	{ .data = { 0x34, 0x16 } },
	{ .data = { 0xCA, 0xCB } },
	{ .data = { 0x43, 0xCD } },
	{ .data = { 0x0E, 0x4B } },
	{ .data = { 0x4B, 0x20 } },
	{ .data = { 0x19, 0x6B } },
	{ .data = { 0x06, 0xB3 } },
	{ .data = { 0xD2, 0xE3 } },
	{ .data = { 0x2B, 0x38 } },
	{ .data = { 0x00, 0xD4 } },
	{ .data = { 0x00, 0x01 } },
	{ .data = { 0x00, 0x0E } },
	{ .data = { 0x04, 0x44 } },
	{ .data = { 0x08, 0x10 } },
	{ .data = { 0x00, 0x00 } },
	{ .data = { 0x00, 0xE6 } },
	{ .data = { 0x80, 0x01 } },
	{ .data = { 0xFF, 0xFF } },
	{ .data = { 0xFF, 0xFF } },
	{ .data = { 0xFF, 0xFF } },
	{ .data = { 0xF0, 0x12 } },
	{ .data = { 0x03, 0x20 } },
	{ .data = { 0x00, 0xFF } },
	{ .data = { 0xF3, 0x00 } },
};

static const struct jadard_panel_desc radxa_display_8hd_ad002_desc = {
	.mode = {
		.clock		= 70000,

		.hdisplay	= 800,
		.hsync_start	= 800 + 40,
		.hsync_end	= 800 + 40 + 18,
		.htotal		= 800 + 40 + 18 + 20,

		.vdisplay	= 1280,
		.vsync_start	= 1280 + 20,
		.vsync_end	= 1280 + 20 + 4,
		.vtotal		= 1280 + 20 + 4 + 20,

		.width_mm	= 127,
		.height_mm	= 199,
		.type		= DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED,
	},
	.lanes = 4,
	.format = MIPI_DSI_FMT_RGB888,
	.init_cmds = radxa_display_8hd_ad002_init_cmds,
	.num_init_cmds = ARRAY_SIZE(radxa_display_8hd_ad002_init_cmds),
};

static const struct jadard_init_cmd cz101b4001_init_cmds[] = {
        { .data = { 0xE0, 0xAB } },
        { .data = { 0xBA, 0xE1 } },
        { .data = { 0xBA, 0xAB } },
        { .data = { 0xB1, 0x10 } },
        { .data = { 0x01, 0x47 } },
        { .data = { 0xFF, 0xB2 } },
        { .data = { 0x0C, 0x14 } },
        { .data = { 0x04, 0x50 } },
        { .data = { 0x50, 0x14 } },
        { .data = { 0xB3, 0x56 } },
        { .data = { 0x53, 0x00 } },
        { .data = { 0xB4, 0x33 } },
        { .data = { 0x30, 0x04 } },
        { .data = { 0xB6, 0xB0 } },
        { .data = { 0x00, 0x00 } },
        { .data = { 0x10, 0x00 } },
        { .data = { 0x10, 0x00 } },
        { .data = { 0xB8, 0x05 } },
        { .data = { 0x12, 0x29 } },
        { .data = { 0x49, 0x48 } },
        { .data = { 0x00, 0x00 } },
        { .data = { 0xB9, 0x7C } },
        { .data = { 0x65, 0x55 } },
        { .data = { 0x49, 0x46 } },
        { .data = { 0x36, 0x3B } },
        { .data = { 0x24, 0x3D } },
        { .data = { 0x3C, 0x3D } },
        { .data = { 0x5C, 0x4C } },
        { .data = { 0x55, 0x47 } },
        { .data = { 0x46, 0x39 } },
        { .data = { 0x26, 0x06 } },
        { .data = { 0x7C, 0x65 } },
        { .data = { 0x55, 0x49 } },
        { .data = { 0x46, 0x36 } },
        { .data = { 0x3B, 0x24 } },
        { .data = { 0x3D, 0x3C } },
        { .data = { 0x3D, 0x5C } },
        { .data = { 0x4C, 0x55 } },
        { .data = { 0x47, 0x46 } },
        { .data = { 0x39, 0x26 } },
        { .data = { 0x06, 0xC0 } },
        { .data = { 0xFF, 0x87 } },
        { .data = { 0x12, 0x34 } },
        { .data = { 0x44, 0x44 } },
        { .data = { 0x44, 0x44 } },
        { .data = { 0x98, 0x04 } },
        { .data = { 0x98, 0x04 } },
        { .data = { 0x0F, 0x00 } },
        { .data = { 0x00, 0xC1 } },
        { .data = { 0xC1, 0x54 } },
        { .data = { 0x94, 0x02 } },
        { .data = { 0x85, 0x9F } },
        { .data = { 0x00, 0x7F } },
        { .data = { 0x00, 0x54 } },
        { .data = { 0x00, 0xC2 } },
        { .data = { 0x17, 0x09 } },
        { .data = { 0x08, 0x89 } },
        { .data = { 0x08, 0x11 } },
        { .data = { 0x22, 0x20 } },
        { .data = { 0x44, 0xFF } },
        { .data = { 0x18, 0x00 } },
        { .data = { 0xC3, 0x86 } },
        { .data = { 0x46, 0x05 } },
        { .data = { 0x05, 0x1C } },
        { .data = { 0x1C, 0x1D } },
        { .data = { 0x1D, 0x02 } },
        { .data = { 0x1F, 0x1F } },
        { .data = { 0x1E, 0x1E } },
        { .data = { 0x0F, 0x0F } },
        { .data = { 0x0D, 0x0D } },
        { .data = { 0x13, 0x13 } },
        { .data = { 0x11, 0x11 } },
        { .data = { 0x00, 0xC4 } },
        { .data = { 0x07, 0x07 } },
        { .data = { 0x04, 0x04 } },
        { .data = { 0x1C, 0x1C } },
        { .data = { 0x1D, 0x1D } },
        { .data = { 0x02, 0x1F } },
        { .data = { 0x1F, 0x1E } },
        { .data = { 0x1E, 0x0E } },
        { .data = { 0x0E, 0x0C } },
        { .data = { 0x0C, 0x12 } },
        { .data = { 0x12, 0x10 } },
        { .data = { 0x10, 0x00 } },
        { .data = { 0xC6, 0x2A } },
        { .data = { 0x2A, 0xC8 } },
        { .data = { 0x21, 0x00 } },
        { .data = { 0x31, 0x42 } },
        { .data = { 0x34, 0x16 } },
        { .data = { 0xCA, 0xCB } },
        { .data = { 0x43, 0xCD } },
        { .data = { 0x0E, 0x4B } },
        { .data = { 0x4B, 0x20 } },
        { .data = { 0x19, 0x6B } },
        { .data = { 0x06, 0xB3 } },
        { .data = { 0xD2, 0xE3 } },
        { .data = { 0x2B, 0x38 } },
        { .data = { 0x00, 0xD4 } },
        { .data = { 0x00, 0x01 } },
        { .data = { 0x00, 0x0E } },
        { .data = { 0x04, 0x44 } },
        { .data = { 0x08, 0x10 } },
        { .data = { 0x00, 0x00 } },
        { .data = { 0x00, 0xE6 } },
        { .data = { 0x80, 0x01 } },
        { .data = { 0xFF, 0xFF } },
        { .data = { 0xFF, 0xFF } },
        { .data = { 0xFF, 0xFF } },
        { .data = { 0xF0, 0x12 } },
        { .data = { 0x03, 0x20 } },
        { .data = { 0x00, 0xFF } },
        { .data = { 0xF3, 0x00 } },
};

static const struct jadard_panel_desc cz101b4001_desc = {
	.mode = {
		.clock		= 66000,,

		.hdisplay	= 800,
		.hsync_start	= 800 + 64,
		.hsync_end	= 800 + 64 + 16,
		.htotal		= 800 + 64 + 16 + 64,

		.vdisplay	= 1280,
		.vsync_start	= 1280 + 2,
		.vsync_end	= 1280 + 2 + 4,
		.vtotal		= 1280 + 2 + 4 + 12,

		.width_mm	= 135,
		.height_mm	= 216,
		.type		= DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED,
	},
	.lanes = 4,
	.format = MIPI_DSI_FMT_RGB888,
	.init_cmds = cz101b4001_init_cmds,
	.num_init_cmds = ARRAY_SIZE(cz101b4001_init_cmds),
};

static int jadard_dsi_probe(struct mipi_dsi_device *dsi)
{
	struct device *dev = &dsi->dev;
	const struct jadard_panel_desc *desc;
	struct jadard *jadard;
	int ret;

	jadard = devm_kzalloc(&dsi->dev, sizeof(*jadard), GFP_KERNEL);
	if (!jadard)
		return -ENOMEM;

	desc = of_device_get_match_data(dev);
	dsi->mode_flags = MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_BURST |
			  MIPI_DSI_MODE_NO_EOT_PACKET;
	dsi->format = desc->format;
	dsi->lanes = desc->lanes;

	jadard->reset = devm_gpiod_get(dev, "reset", GPIOD_OUT_LOW);
	if (IS_ERR(jadard->reset)) {
		DRM_DEV_ERROR(&dsi->dev, "failed to get our reset GPIO\n");
		return PTR_ERR(jadard->reset);
	}

	jadard->vdd = devm_regulator_get(dev, "vdd");
	if (IS_ERR(jadard->vdd)) {
		DRM_DEV_ERROR(&dsi->dev, "failed to get vdd regulator\n");
		return PTR_ERR(jadard->vdd);
	}

	jadard->vccio = devm_regulator_get(dev, "vccio");
	if (IS_ERR(jadard->vccio)) {
		DRM_DEV_ERROR(&dsi->dev, "failed to get vccio regulator\n");
		return PTR_ERR(jadard->vccio);
	}

	drm_panel_init(&jadard->panel, dev, &jadard_funcs,
		       DRM_MODE_CONNECTOR_DSI);

	ret = drm_panel_of_backlight(&jadard->panel);
	if (ret)
		return ret;

	drm_panel_add(&jadard->panel);

	mipi_dsi_set_drvdata(dsi, jadard);
	jadard->dsi = dsi;
	jadard->desc = desc;

	ret = mipi_dsi_attach(dsi);
	if (ret < 0)
		drm_panel_remove(&jadard->panel);

	return ret;
}

static void jadard_dsi_remove(struct mipi_dsi_device *dsi)
{
	struct jadard *jadard = mipi_dsi_get_drvdata(dsi);

	mipi_dsi_detach(dsi);
	drm_panel_remove(&jadard->panel);
}

static const struct of_device_id jadard_of_match[] = {
	{
		.compatible = "chongzhou,cz101b4001",
		.data = &cz101b4001_desc
	},
	{
		.compatible = "radxa,display-10hd-ad001",
		.data = &cz101b4001_desc
	},
	{
                .compatible = "pine64,pinetab-v-panel",
                .data = &cz101b4001_desc
        },
	{
		.compatible = "radxa,display-8hd-ad002",
		.data = &radxa_display_8hd_ad002_desc
	},
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, jadard_of_match);

static struct mipi_dsi_driver jadard_driver = {
	.probe = jadard_dsi_probe,
	.remove = jadard_dsi_remove,
	.driver = {
		.name = "jadard-jd9365da",
		.of_match_table = jadard_of_match,
	},
};
module_mipi_dsi_driver(jadard_driver);

MODULE_AUTHOR("Jagan Teki <jagan@edgeble.ai>");
MODULE_AUTHOR("Stephen Chen <stephen@radxa.com>");
MODULE_DESCRIPTION("Jadard JD9365DA-H3 WXGA DSI panel");
MODULE_LICENSE("GPL");
