# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Tool to communicate with Qualcomm System On a Chip bootroms to install or execute code"
HOMEPAGE="https://github.com/andersson/qdl"
SRC_URI="https://github.com/andersson/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~arm64"

DEPEND="dev-libs/libxml2"
RDEPEND="${DEPEND}"
BDEPEND=""
