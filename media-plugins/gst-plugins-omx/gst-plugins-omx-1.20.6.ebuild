# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit gstreamer-meson

MY_PN="gst-omx"
MY_PV="$(ver_cut 1-3)"
MY_P="${MY_PN}-${MY_PV}"

DESCRIPTION="OpenMax-based decoder and encoder elements for GStreamer"
HOMEPAGE="https://gstreamer.freedesktop.org/modules/gst-omx.html"
SRC_URI="https://gstreamer.freedesktop.org/src/${MY_PN}/${MY_P}.tar.xz"
S="${WORKDIR}/${MY_P}"

LICENSE="LGPL-2+"
SLOT="1.0"
KEYWORDS="~amd64 ~arm64 ~riscv"

RDEPEND="
	>=dev-libs/glib-2.40.0:2[${MULTILIB_USEDEP}]
	>=media-libs/gstreamer-${MY_PV}:1.0[${MULTILIB_USEDEP}]
	>=media-libs/gst-plugins-base-${MY_PV}:1.0[${MULTILIB_USEDEP}]
"
DEPEND="${RDEPEND}"
BDEPEND=""

multilib_src_configure() {
	local emesonargs=()

	emesonargs+=( 
		-Dtarget=generic
	)
	meson_src_configure
}
