#!/bin/bash
rm -rf /tmp/boot
xmake INSTALL_DTBS_PATH=/tmp/boot dtbs_install
xmake INSTALL_MOD_PATH=/tmp/boot modules_install
cp Module.symvers $(ls -d /tmp/boot/lib/modules/*)
lz4 arch/arm64/boot/Image
mv arch/arm64/boot/Image.lz4 /tmp/boot
if ping -c1 192.168.1.107
then
  rsync -avP /tmp/boot 192.168.1.107:/tmp
elif ping -c1 192.168.2.107
then
  rsync -avP /tmp/boot 192.168.2.107:/tmp
elif ping -c1 192.168.1.249
then
  rsync -avP /tmp/boot 192.168.2.249:/tmp
fi
