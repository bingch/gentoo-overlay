if [[ $# < 1 ]]
then
  echo "Usage $0 ver"
  exit
else
#ver=$(uname -r)
  ver=$1
fi
echo $ver
if [ -f /tmp/boot/Image.lz4 ]
then
  cp /tmp/boot/Image.lz4 /boot/Image.lz4-$ver
fi

if [ -d /tmp/boot/qcom ]
then
  if [ -d /boot/dtbs/$ver ]
  then
    rm -rf /boot/dtbs/$ver
  fi

  mkdir -p /boot/dtbs/$ver
  mv /tmp/boot/qcom /boot/dtbs/$ver
fi

if [ -d /tmp/boot/lib/modules/$ver ]
then
  if [ -d /lib/modules/$ver ]
  then
    rm -r /lib/modules/$ver
  fi
  mv /tmp/boot/lib/modules/$ver /lib/modules/$ver
fi
depmod -a $ver
dracut -f -H --xz --no-hostonly-cmdline "" $ver

cd /boot
dd if=/dev/zero of=bootloader.bin bs=512 count=1

for i in $(find /boot/dtbs/${ver}/qcom/ -name sc7180-trogdor-homesta*)
  do opt_b="${opt_b} -b ${i}"
done
 
#-b /boot/dtbs/${ver}/qcom/sc7180-idp.dtb \
#-b /boot/dtbs/${ver}/qcom/sc7180-trogdor-r1.dtb \
#-b /boot/dtbs/${ver}/qcom/sc7180-trogdor-r1-lte.dtb \
mkimage -D "-I dts -O dtb -p 2048" -f auto -A arm64 -O linux -T kernel -C lz4 -a 0 -d /boot/Image.lz4-${ver} ${opt_b} -i /boot/initramfs-${ver}.img /tmp/kernel.itb
vbutil_kernel --pack /tmp/vmlinux.kpart --keyblock /usr/share/vboot/devkeys/kernel.keyblock --signprivate /usr/share/vboot/devkeys/kernel_data_key.vbprivk --version 1 --config cmdline-sd --bootloader bootloader.bin --vmlinuz /tmp/kernel.itb --arch arm
mv /tmp/vmlinux.kpart /boot/vmlinux.kpart-${ver}
