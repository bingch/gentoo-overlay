#!/bin/bash
rm -rf /tmp/boot
xmake-risc INSTALL_DTBS_PATH=/tmp/boot dtbs_install
xmake-risc INSTALL_MOD_PATH=/tmp/boot modules_install
xmake-risc INSTALL_PATH=/tmp/boot zinstall
cp Module.symvers $(ls -d /tmp/boot/lib/modules/*)
if ping -c1 192.168.1.208
then
  rsync -avP /tmp/boot 192.168.1.208:/tmp
elif ping -c1 192.168.2.155
then
  rsync -avP /tmp/boot 192.168.2.155:/tmp
fi
