gentoo-overlay
==============

## Adding local repo using eselect (need app-eselect/eselect-repository installed)

sudo eselect repository add bingch git https://gitlab.com/bingch/gentoo-overlay.git

## Adding the overlay using layman

First create a local overlay list:

```sh
echo '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE repositories SYSTEM "/dtd/repositories.dtd">
<repositories xmlns="" version="1.0">
  <repo quality="experimental" status="unofficial">
    <name>bingch</name>
    <description>bingch local ebuilds</description>
    <homepage>https://gitlab.com/bingch/gentoo-overlay</homepage>
    <owner type="project">
       <email>bingquick@hotmail.com</email>
       <name>Bing Chen</name>
    </owner>
    <source type="git">https://gitlab.com/bingch/gentoo-overlay.git</source>
    <feed>https://gitlab.com/feeds/bingch/commits/gentoo-overlay/master</feed>
  </repo>
</repositories>' > /etc/layman/overlays/bingch.xml
```

Then make it known to layman and add it to your local overlays:

```sh
layman -L
layman -a bingch
```

## Setup for pinephone https://www.pine64.org/pinephone/

* follow https://wiki.gentoo.org/wiki/Cross_build_environment to setup cross build enviroment in your gentoo host
* cross compile @system, rsync -azvP /usr/aarch64-unknown-linux-gnu/ /root/partition/SD
* cross compile kernel https://wiki.gentoo.org/wiki/Embedded_Handbook/General/Cross-compiling_the_kernel, either use upstream 5.18 kernel with mergi's patch in https://xff.cz/kernels/, or just download the compiled one, which seems to be needed if you choose to use p-boot, or the original https://gitlab.com/pine64-org/linux for u-boot 
* cross compile p-boot https://xnux.eu/p-boot/ or u-boot https://linux-sunxi.org/U-Boot install kernel/dtbs/etc to boot partition
* put SD card in pinephone and to test boot pinephone to verify basic setup works ok
* aarch64-unknown-linux-gnu-emerge pinephone phosh-meta -av --keep-going should pull in all pkgs to start phosh. Not all packages are cross compilable, you will need to build the failed one in chroot or on the phone itself

## Setup for duet 5
* mainline kernel seems has some issue with suspend, system comes backup from suspend mode after couple secs, use google's chromeos 5.4/5.15 patched kernel (https://chromium.googlesource.com/chromiumos/third_party/kernel) works fine
* WIFI needs rmtfs service (https://github.com/andersson/rmtfs) and firmware and /var/lib/rmsfs from chromeos to work
* internal speaker only works with pro-audio profile with 4 ch sink, you need to change "PlaybackChannels" from 2 to 4 in /usr/share/alsa/ucm2/Qualcomm/sc7180/rt5682-max98357a/HiFi.conf file. You will also need to rearrange 4 speakers' setup as by default it's for portrait mode (FL/FR on power button side, RL/RR on other side), I used pipewire loopback module to re-arrange them for landscale mode. See etc/pipewire/pipewire.conf file
* usbc video output seems only support maximum 2560x14440@60HZ, edit ~/.config/monitors.xml file monitor section will fix gnome display issue when 4K monitor is accidentally set to use native resolution
* haven't figure out how to make camera working yet

## Setup for StarFive VF2 / JH7110
* use kernel/boot partitons/firmware from official image from https://debian.starfivetech.com/, or build custom kernel from sys-kernel/visionfive2-sources from this overlay
* to build the starfive patched 5.15.0 kernel, gcc-10/binutils-2.35 are needed. Also need to include gpu firmwares in the initrd file, check the pvr.conf for dracut installed by media-libs/img-gpu-powervr-bin in this overlay
* After succeessfully build custom kernel, you need to emerge media-video/vf2vpudev from this overlay to build the out-of-tree kernel modules jpu/venc/vdec
* emerge pkgs as usaual, except for mesa, need to use patched mesa-22.1.7 from this overlay, it will also emerge media-libs/img-gpu-powervr-bin binary pkg
* until https://bugs.gentoo.org/909828 is resolved, need apply the patch in this overlay etc/portage-riscv/etc/patches/gnome-base/gnome-shell-44.2/0001-Correct-the-pointer-value-of-strtab-on-riscv-mips.patch for gnome-shell to workaround gnome-shell signal 11 segfault

## Setup for PineTab V 
* similar to StarFive 2 as it use the same JH7110 SOC, the official kernel source of it seems to be a patched visionfive2 5.15.0 VF2_v2.11.5 with additional patches for the wifi and camera sensors. It will need to use img-gpu-powervr-bin-1.17.6210866 ebuild to match the VF2_v2.11.5 branch or replace drivers/gpu/drm/img folder with VF2_v3.1.5 one to use img-gpu-powervr-bin-1.19.6345021 ebuild
