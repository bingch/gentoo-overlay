# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="Firmware files for spacemit k1x vpu"
HOMEPAGE="https://gitee.com/bianbu-linux/k1x-vpu-firmware"
EGIT_REPO_URI="https://gitee.com/bianbu-linux/k1x-vpu-firmware.git"


LICENSE=""
SLOT="0"
KEYWORDS="~riscv"
IUSE=""

src_install() {
	insinto /lib/firmware/
	doins -r lib/firmware/*
}
