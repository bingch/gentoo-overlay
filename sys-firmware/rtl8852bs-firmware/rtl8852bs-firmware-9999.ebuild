# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 systemd

DESCRIPTION="Firmware files for RTL8852BS"
HOMEPAGE="https://github.com/radxa/rtkbt"
EGIT_REPO_URI="https://github.com/radxa/rtkbt.git"

LICENSE=""
SLOT="0"
KEYWORDS="~riscv"
IUSE="rtlwifi"


src_compile() {
	cd ${S}
	make -C uart/rtk_hciattach
}

src_install() {
	for sub in rtl_bt rtlbt
	do
		insinto /lib/firmware/${sub}
		doins ${S}/rtkbt-firmware/lib/firmware/${sub}/rtl8852bs*
	done
}
