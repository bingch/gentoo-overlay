# Copyright 2019-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit linux-mod systemd

DESCRIPTION="VisionFive 2 Video Process Unit drivers"
HOMEPAGE="https://github.com/starfive-tech/soft_3rdpart"
SRC_URI="https://github.com/starfive-tech/soft_3rdpart/archive/refs/tags/JH7110_VF2_6.1_v${PV}.zip"
S="${WORKDIR}/soft_3rdpart-JH7110_VF2_6.1_v${PV}"

KEYWORDS="~riscv"
LICENSE="restricted"
SLOT="0"

# Requires connection to phone/tablet
RESTRICT="test"

CONFIG_CHECK="VIDEO_DEV MEDIA_SUPPORT MEDIA_CAMERA_SUPPORT"

PATCHES=(
)

pkg_setup() {
	local jpu_path=${S}/codaj12/jdi/linux/driver
	local venc_path=${S}/wave420l/code/vdi/linux/driver
	local vdec_path=${S}/wave511/code/vdi/linux/driver
	MODULE_NAMES="jpu(extra:${jpu_path}:${jpu_path})
	venc(extra:${venc_path}:${venc_path})
	vdec(extra:${vdec_path}:${vdec_path})
	"
	linux-mod_pkg_setup
}

src_configure() {
	set_arch_to_kernel
	default
}

src_compile() {
	BUILD_PARAMS="KERNELDIR=${KERNEL_DIR}"
	BUILD_TARGETS="clean default"
	linux-mod_src_compile
}

src_install() {
	linux-mod_src_install
	cd ${S}/wave420l/firmware
	insinto /lib/firmware
	doins monet.bin
	cd ${S}/wave511/firmware
	insinto /lib/firmware
	doins chagall.bin
	doinitd ${FILESDIR}/vpudev
	systemd_dounit ${FILESDIR}/vpudev.service
}

pkg_postinst() {
	systemd_reenable vpudev.service
}

