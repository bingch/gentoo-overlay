# Copyright 2019-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="OpenMAX library implementation for VisionFive via WAVE420l WAVE511 and CODAJ12"
HOMEPAGE="https://github.com/starfive-tech/soft_3rdpart"
SRC_URI="https://github.com/starfive-tech/soft_3rdpart/archive/refs/tags/JH7110_VF2_6.1_v${PV}.zip"
S="${WORKDIR}/soft_3rdpart-JH7110_VF2_6.1_v${PV}"

KEYWORDS="~riscv"
LICENSE="GPL-2"
SLOT="0"

# Requires connection to phone/tablet
RESTRICT="test"

DEPEND="
	media-video/vf2vpulib
"
PATCHES=(
)

S=${WORKDIR}/soft_3rdpart-JH7110_VF2_6.1_v${PV}/omx-il

src_prepare() {
	eapply_user
	# fix suffix and ignore warning
	sed -i -e s/buildroot-linux/unknown-linux/ -e s/Werror/Wno-error/ \
	Makefile
	# use customized cmake from 
	# https://raw.githubusercontent.com/riscv/meta-riscv/master/recipes-multimedia/vpu/libsf-omxil/CMakeLists.txt
	cp "${FILESDIR}/CMakeLists.txt" ${S}
	cmake_src_prepare
}

src_install() {
	cmake_src_install
	ln -s -r -f ${D}/usr/lib64/libsf-omx-il.so ${D}/usr/lib64/libOMX_Core.so
}
