# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake systemd
DESCRIPTION="Spacemit K1x AI support"
HOMEPAGE="https://gitee.com/bianbu-linux/ai-support"

LICENSE="BSD"
SLOT="0"
IUSE=""

DEPEND="${RDEPEND}
	media-libs/opencv
"

if [[ ${PV} == 9999 ]]; then
	EGIT_REPO_URI="https://gitee.com/bianbu-linux/${PN}.git"
    inherit git-r3
else
	SRC_URI="https://gitee.com/bianbu-linux/${PN}/repository/archive/v${PV} -> ${PN}-v${PV}.zip"
    KEYWORDS="~riscv"
	S=${WORKDIR}/${PN}-v${PV}
fi
