# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit gnome2-utils meson xdg udev
DESCRIPTION="A camera app that deal with V4L2 request"
HOMEPAGE="https://git.sr.ht/~martijnbraam/megapixels"
SRC_URI="https://gitlab.com/postmarketOS/megapixels/-/archive/${PV}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~arm64"
IUSE=""

DEPEND="media-libs/tiff
		gui-libs/gtk:4
		media-gfx/dcraw
		media-gfx/zbar
		media-gfx/imagemagick
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	default
	meson_src_install
	udev_dorules ${FILESDIR}/90-megapixels.rules
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}

