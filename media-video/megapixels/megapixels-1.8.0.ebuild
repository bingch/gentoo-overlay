# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit gnome2-utils meson xdg udev
DESCRIPTION="A GTK4 camera application that knows how to deal with the media request API"
HOMEPAGE="https://gitlab.com/megapixels-org/Megapixels"
SRC_URI="https://gitlab.com/megapixels-org/Megapixels/-/archive/${PV}/Megapixels-${PV}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~arm64"
IUSE=""

PATCHES=(
   ${FILESDIR}/06230f3a02cffdf8b683f85cb32fc256d73615d9.patch
   ${FILESDIR}/27a1e606d680295e0b4caceadf74ff5857ac16b2.patch
   ${FILESDIR}/d8b35bc223989cb165ba1b0716ab9f0ca9c43e53.patch
)

DEPEND="media-libs/tiff
		media-libs/libepoxy
		gui-libs/gtk:4
		media-gfx/dcraw
		media-gfx/zbar
		media-gfx/imagemagick
		dev-libs/feedbackd
"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}/Megapixels-${PV}

src_install() {
	default
	meson_src_install
	udev_dorules ${FILESDIR}/90-megapixels.rules
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}

