# Copyright 2019-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools

DESCRIPTION="VisionFive 2 Video Process Unit libraries"
HOMEPAGE="https://github.com/starfive-tech/soft_3rdpart"
SRC_URI="https://github.com/starfive-tech/soft_3rdpart/archive/refs/tags/JH7110_VF2_6.6_v${PV}.zip"
S="${WORKDIR}/soft_3rdpart-JH7110_VF2_6.6_v${PV}"

KEYWORDS="~riscv"
LICENSE="restricted"
SLOT="0"

# Requires connection to phone/tablet
RESTRICT="test"

PATCHES=(
)

S=${WORKDIR}/soft_3rdpart-JH7110_VF2_6.6_v${PV}

src_configure() {
	# fix suffix and ignore warning
	sed -i -e s/buildroot-linux/unknown-linux/ -e s/Werror/Wno-error/ \
		"${S}/codaj12/codaj12_buildroot.mak" \
		"${S}/wave420l/code/WaveEncoder_buildroot.mak" \
		"${S}/wave511/code/WaveDecode_buildroot.mak" \
	# # workaround wrong build system/include path assumptions
	for i in wave20l wave511; do
		cd ${S}/${i}/code; ln -s $PWD $i
	done
}

src_compile() {
	cd ${S}/codaj12
	emake -f codaj12_buildroot.mak || die
	cd ${S}/wave420l/code
	emake -f WaveEncoder_buildroot.mak || die
	cd ${S}/wave511/code
	emake -f WaveDecode_buildroot.mak || die
	#emake
}

src_install() {
	dolib.so ${S}/codaj12/libcodadec.so
	dolib.so ${S}/wave420l/code/libsfenc.so
	dolib.so ${S}/wave511/code/libsfdec.so

	for i in codaj12 wave420l wave511; do
		if [ $i = 'codaj12' ]; then
			cd ${S}/${i} || die
		else
			cd ${S}/${i}/code || die
		fi
		for h in $(find . -name '*.h'); do
			h_dir=$(dirname $h)
			mkdir -p ${D}/usr/include/${i}/$h_dir || die
			cp $h ${D}/usr/include/${i}/$h_dir || die
        done
	done
}
