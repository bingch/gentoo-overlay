# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit git-r3 toolchain-funcs

DESCRIPTION="Userspace interface to RmNet driver"
HOMEPAGE="https://source.codeaurora.org/quic/lc/chromiumos/third_party/librmnetctl"
EGIT_REPO_URI="https://source.codeaurora.org/quic/lc/chromiumos/third_party/${PN}"
EGIT_BRANCH="LC.UM.1.0"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~arm64"

src_prepare() {
	default
}

src_install() {
	emake prefix="${ED}/usr" libdir="\$(prefix)/$(get_libdir)" install
}
