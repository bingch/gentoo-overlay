# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="RISC-V Open Source Supervisor Binary Interface for spacemit k1"
HOMEPAGE="https://github.com/BPI-SINOVOIP/pi-opensbi"
EGIT_REPO_URI="https://github.com/BPI-SINOVOIP/pi-opensbi.git"
EGIT_BRANCH="v1.3-k1"

LICENSE="BSD 2"
SLOT="0"
KEYWORDS="~riscv"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="sys-apps/dtc
	dev-embedded/u-boot-tools
"
