# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson git-r3
DESCRIPTION="Framebuffer-based disk unlocker for the initramfs based on LVGL"
HOMEPAGE="https://gitlab.com/postmarketOS/buffybox/-/tree/master/unl0kr"
EGIT_REPO_URI="https://gitlab.com/postmarketOS/buffybox.git"
#SRC_URI="https://gitlab.com/postmarketOS/buffybox/-/archive/${PV}/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~riscv"
IUSE="test"

if [[ ${PV} != 9999 ]]; then
        EGIT_COMMIT="tags/${PV}"
else
        KEYWORDS=""
fi

DEPEND="sys-fs/cryptsetup
		dev-libs/inih
		dev-libs/libinput
		x11-libs/libdrm
		x11-libs/libxkbcommon
"

RDEPEND="${DEPEND}"
BDEPEND="app-text/scdoc
"

S=${S}/${PN}

src_configure() {
	meson setup --prefix=/usr _build
}

src_compile() {
	meson compile -j$(nproc) -C _build
}

src_install() {
	DESTDIR="${D}" meson install --no-rebuild -C _build
	insinto /etc
	doins unl0kr.conf
	insinto /etc/dracut.conf.d/
	doins ${FILESDIR}/unl0kr.conf
	insinto /usr/lib/dracut/modules.d/50unl0kr
	doins ${FILESDIR}/unlock-root.path
	doins ${FILESDIR}/unlock-root.service
	exeinto /usr/lib/dracut/modules.d/50unl0kr
	doexe ${FILESDIR}/module-setup.sh
	doexe ${FILESDIR}/unlock-root.sh
	doexe ${FILESDIR}/unlock.sh
}

pkg_postinst() {
	einfo "For more info on how to test ${PN} and how to report problems, see:"
	einfo "${HOMEPAGE}"
	einfo "To use ${PN} to unlock encrypted root at bootime, check ${PN}.conf in /etc/dracut.conf.d"
	einfo "and add these boot option 'root=/dev/mapper/root cryptroot=/dev/path/to/encrypted_partition' "
}
