#!/bin/bash
# stole from postmarket
# https://gitlab.com/postmarketOS/pmaports/-/blob/master/main/postmarketos-mkinitfs/init_functions.sh
find_crypt_partition() {
	# The partition layout is one of the following:
	# a) boot, root partitions on sdcard
	# b) boot, root partition on the "system" partition (which has its
	#    own partition header! so we have partitions on partitions!)
	#
	# mount_subpartitions() must get executed before calling
	# find_root_partition(), so partitions from b) also get found.

	# Short circuit all autodetection logic if pmos_root= is supplied
	# on the kernel cmdline
	# shellcheck disable=SC2013
	for x in $(cat /proc/cmdline); do
		[ "$x" = "${x#cryptroot=}" ] && continue
		DEVICE="${x#cryptroot=}"
	done
	echo "$DEVICE"
}

start_onscreen_keyboard() {
	if [ -z $partition ]
	then
		exit 0
	fi
	echo "unlocking $partition"
	if [ -b $partition ]
	then
	  /usr/bin/unlock.sh "$partition" root
	else
	  echo "wait 10 sec for $partition to become available..."
	  sleep 10
	  if [ -b $partition ]
	  then
	    /usr/bin/unlock.sh "$partition" root
	  else
	    echo "$partition is not available, give up"
	  fi
	fi
}

unlock_root_partition() {
	partition="$(find_crypt_partition)"
	start_onscreen_keyboard
}

unlock_root_partition
