#!/bin/bash

# called by dracut
check() {
    return 0
}

# called by dracut
depends() {
    echo dm
    return 0
}

# called by dracut
install() {
    inst_multiple "/etc/unl0kr.conf" \
       "/usr/lib/udev/rules.d/*" \
       "/usr/share/libinput/*.quirks" \
       "/usr/share/X11/xkb/*/*" \
       cryptsetup unl0kr
    inst_simple $moddir/unlock.sh /usr/bin/unlock.sh
    inst_simple $moddir/unlock-root.sh /usr/bin/unlock-root.sh
    inst_simple $moddir/unlock-root.service ${systemdsystemunitdir}/unlock-root.service
    inst_simple $moddir/unlock-root.path ${systemdsystemunitdir}/unlock-root.path
    systemctl -q --root "$initdir" add-wants sysinit.target unlock-root.path
}
