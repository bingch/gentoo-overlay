#!/bin/bash 
if [ ! $# -eq 2 ]
then
  exit -1
fi
retries=3
while [ ! -b /dev/mapper/$2  ]
do
  unl0kr | cryptsetup  --perf-no_read_workqueue --perf-no_write_workqueue open "$1" "$2" -
  sleep 2
  retries=$((retries - 1))
  if [ $retries -le 0 ]
  then
    exit -1
  fi
done  
