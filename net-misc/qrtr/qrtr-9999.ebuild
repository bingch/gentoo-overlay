# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3
DESCRIPTION="Userspace reference for net/qrtr in the Linux kernel"
HOMEPAGE="https://github.com/andersson/qrtr"
EGIT_REPO_URI="https://github.com/andersson/${PN}.git"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~arm64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install(){
	make prefix=/usr libdir=/usr/lib64 install DESTDIR="${D}"
}
