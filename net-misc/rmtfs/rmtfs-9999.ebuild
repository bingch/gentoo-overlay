# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 systemd
DESCRIPTION="Qualcomm Remote Filesystem Service Implementation"
HOMEPAGE="https://github.com/andersson/rmtfs"
EGIT_REPO_URI="https://github.com/andersson/${PN}.git"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~arm64"

DEPEND="net-misc/qrtr"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install(){
	make prefix=/usr libdir=/usr/lib64 install DESTDIR="${D}"
	# use our own rmtfs.service
	rm -rf ${D}/usr/lib/systemd
	systemd_dounit "${FILESDIR}"/rmtfs.service
}

pkg_postinst() {
	einfo "rmtfs.service need to run to enable the modem for duet 5"
	einfo "it also need /lib/firmware and /var/lib/rmtfs from chromos"
	einfo "to work properly, run qrtr-lookup should show a list of"
	einfo "service incluing ATH10k WLAN firmware service"
}
