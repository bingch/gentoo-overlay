# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Qualcomm modem data service"
HOMEPAGE="https://source.codeaurora.org/quic/dataservices/modem-data-manager/log/?h=LC.UM.1.0"
EGIT_REPO_URI="https://source.codeaurora.org/quic/dataservices/modem-data-manager"
EGIT_BRANCH="LC.UM.1.0"
LICENSE="BSD"
SLOT="0"
KEYWORDS="~arm64"
IUSE=""

DEPEND="net-libs/librmnetctl
	net-misc/qrtr
"

RDEPEND="${DEPEND}"

src_install() {
	emake DESTDIR="${D}" prefix="${EPREFIX}/usr" install
}
