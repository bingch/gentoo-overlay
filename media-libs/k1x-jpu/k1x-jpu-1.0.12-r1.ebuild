# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake systemd
DESCRIPTION="Spacemit K1x jpeg process unit"
HOMEPAGE="https://gitee.com/bianbu-linux/k1x-jpu"

LICENSE="BSD"
SLOT="0"
IUSE=""

if [[ ${PV} == 9999 ]]; then
	EGIT_REPO_URI="https://gitee.com/bianbu-linux/${PN}.git"
    inherit git-r3
else
	SRC_URI="https://gitee.com/bianbu-linux/${PN}/repository/archive/v${PV} -> ${PN}-v${PV}.zip"
    KEYWORDS="~riscv"
	S=${WORKDIR}/${PN}-v${PV}
fi

PATCHES=(
	${FILESDIR}/fix-gcc14.patch
)

src_install() {
	cmake_src_install
	doinitd etc/init.d/*

	systemd_dounit usr/lib/systemd/system/*
}

pkg_postinst() {
	systemd_enable_service --global jpu
}
