# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="ALSA ucm configuration files for PinePhone"
HOMEPAGE="https://gitlab.com/pine64-org/pine64-alsa-ucm"
LICENSE="MIT"
SLOT="0"

KEYWORDS="arm64"
IUSE=""

RDEPEND="media-libs/alsa-ucm-conf"
DEPEND="${RDEPEND}"

S=${WORKDIR}

src_install() {
	insinto /usr/share/alsa/ucm2/PinePhone
	doins   ${FILESDIR}/PinePhone.conf
	newins  ${FILESDIR}/PinePhone-HiFi.conf HiFi.conf
	newins  ${FILESDIR}/PinePhone-VoiceCall.conf VoiceCall.conf

	insinto /usr/share/alsa/ucm2/PinePhonePro
	doins   ${FILESDIR}/PinePhonePro.conf
	newins  ${FILESDIR}/PinePhonePro-HiFi.conf HiFi.conf
	newins  ${FILESDIR}/PinePhonePro-VoiceCall.conf VoiceCall.conf

	#link for 1.2.6 and higher
	dodir /usr/share/alsa/ucm2/conf.d/simple-card/
	dosym ../../PinePhone/PinePhone.conf /usr/share/alsa/ucm2/conf.d/simple-card/PinePhone.conf
	dosym ../../PinePhonePro/PinePhonePro.conf /usr/share/alsa/ucm2/conf.d/simple-card/PinePhonePro.conf
}
