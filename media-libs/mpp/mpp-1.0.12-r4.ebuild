# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake udev
DESCRIPTION="Spacemit K1x Multimedia Processing Platform"
HOMEPAGE="https://gitee.com/bianbu-linux/mpp"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~riscv"
IUSE=""

if [[ ${PV} == 9999 ]]; then
    EGIT_REPO_URI="https://gitee.com/bianbu-linux/${PN}.git"
    inherit git-r3
else
    SRC_URI="https://gitee.com/bianbu-linux/${PN}/repository/archive/v${PV} -> ${PN}-v${PV}.zip"
    KEYWORDS="~riscv"
    S=${WORKDIR}/${PN}-v${PV}
fi

DEPEND="media-libs/openh264
	media-video/vf2vpulib
	media-video/vf2omx-il
	media-libs/libpng
	media-libs/libsdl2
"
RDEPEND="${DEPEND}"
BDEPEND="${DEPEND}"

src_install() {
	cmake_src_install
	udev_dorules debian/usr/lib/udev/rules.d/*

	# symlink pkgconfig to /usr/lib64/pkgconfig
	mkdir -p ${D}/usr/lib64/pkgconfig || die
	cd ${D}/usr/lib64/pkgconfig; ln -s ../../lib/pkgconfig/*.pc . || die
}
