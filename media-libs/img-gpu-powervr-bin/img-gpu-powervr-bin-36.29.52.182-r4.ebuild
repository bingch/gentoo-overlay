# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit udev git-r3

DESCRIPTION="Userspace libraries for Imagination PowerGPU GPU on SpacemIT K1"
HOMEPAGE="https://github.com/starfive-tech/soft_3rdpart.git"

EGIT_REPO_URI="https://gitee.com/bianbu-linux/img-gpu-powervr.git"
EGIT_COMMIT="v2.0.4"

LICENSE="restricted"
SLOT="0"
KEYWORDS="~riscv"

IUSE="+vulkan +opencl"

DEPEND="x11-libs/libdrm
	vulkan? ( media-libs/vulkan-loader )
	opencl? ( virtual/opencl )
	!x11-apps/mesa-progs
"
RDEPEND="${DEPEND}"
BDEPEND=""

RESTRICT="strip"

src_install() {
	cd ${S}/target/usr/lib
	# rm files conflicted with libglvnd/vulkan-icd-loader/opencl-icd-loader
	rm libGLESv2.so libvulkan.so libvulkan.so.1 || die
	# rm files conflicted with mesa
	rm libpvr_mesa_wsi.so || die

	insinto /usr/lib64
	doins *

	cd ${S}/staging/include/
	# don't install conflict header files with libglvnd spirv-header
	doheader -r drv

	if use vulkan
	then
	    cd ${S}/target/etc/vulkan/icd.d
    	insinto /usr/share/vulkan/icd.d
	    newins powervr_icd.json icdconf.json
	    cd ${S}/target/etc
		insinto /etc
		doins powervr.ini
	fi

	cd ${S}/target/lib/firmware
	insinto /lib/firmware
	doins *

	insinto /etc/dracut.conf.d
	doins ${FILESDIR}/esos.conf

	cd ${S}/target/usr/local/bin
	dobin *

	#xorg conf for starfive
	#insinto /usr/share/X11/xorg.conf.d
	#doins ${FILESDIR}/10-starfive.conf
	# environment variable setting for starfive
	insinto /etc/environment.d/
	doins ${FILESDIR}/10-starfive-env.conf

	# mutter need to use /dev/dri/card1 as primary
	#udev_dorules ${FILESDIR}/61-mutter-primary-gpu.rules
}

pkg_postinst() {
	udev_reload
}

pkg_postrm() {
	udev_reload
}
