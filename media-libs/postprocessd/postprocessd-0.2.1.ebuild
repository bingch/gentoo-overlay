# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

DESCRIPTION="A native postprocessing pipeline for Megapixels"
HOMEPAGE="https://git.sr.ht/~martijnbraam/postprocessd"
SRC_URI="https://git.sr.ht/~martijnbraam/${PN}/archive/${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~arm64"
IUSE=""

DEPEND="
	media-libs/tiff
	media-gfx/dcraw
	media-gfx/zbar
	media-libs/libexif
	media-libs/libjpeg-turbo
	media-libs/opencv:=[contrib(+)]
"

RDEPEND="${DEPEND}"
BDEPEND=""
