# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit unpacker
DESCRIPTION="A browser plugin designed for the viewing of premium video content"
HOMEPAGE="https://www.widevine.com/"
SRC_URI="https://archive.raspberrypi.org/debian/pool/main/w/widevine/${PN}_${PV}+3_arm64.deb"
LICENSE="custom"
SLOT="0"
KEYWORDS="~aarch64"
pkgver=4.10.2257.0
IUSE=""

DEPEND="
	dev-libs/glib:2
	dev-libs/nspr
	dev-libs/nss
"
RDEPEND="${DEPEND}"
BDEPEND=""

RESTRICT="bindist mirror"
#RESTRICT="nostrip"

QA_PREBUILT="*"
S="${WORKDIR}"

src_unpack() {
	unpack_deb ${A}
}

src_prepare() {
	default
	# Get major and minor version numbers from pkgver string
	local _major_version="$(echo ${pkgver} | cut -d. -f1)"
	local _minor_version="$(echo ${pkgver} | cut -d. -f2)"

	# Construct necessary metadata file(s)
	echo "{" > manifest.json
	echo "   \"name\": \"WidevineCdm\"," >> manifest.json
	echo "   \"description\": \"Widevine Content Decryption Module\"," >> manifest.json
	echo "   \"version\": \"${pkgver}\"," >> manifest.json
	echo "   \"x-cdm-codecs\": \"vp8,vp9.0,avc1,av01\"," >> manifest.json
	echo "   \"x-cdm-host-versions\": \"${_minor_version}\"," >> manifest.json
	echo "   \"x-cdm-interface-versions\": \"${_minor_version}\"," >> manifest.json
	echo "   \"x-cdm-module-versions\": \"${_major_version}\"," >> manifest.json
	echo "   \"x-cdm-persistent-license-support\": true" >> manifest.json
	echo "}" >> manifest.json

	# Create firefox preferences file
	echo "// Set preferences related to widevine loading" > widevine.js
	echo "pref(\"media.gmp-widevinecdm.version\", \"${pkgver}\");" >> widevine.js
	echo "pref(\"media.gmp-widevinecdm.visible\", true);" >> widevine.js
	echo "pref(\"media.gmp-widevinecdm.enabled\", true);" >> widevine.js
	echo "pref(\"media.gmp-widevinecdm.autoupdate\", false);" >> widevine.js
	echo "pref(\"media.eme.enabled\", true);" >> widevine.js
	echo "pref(\"media.eme.encrypted-media-encryption-scheme.enabled\", true);" >> widevine.js
	# patch widevine lib to add missing functions and add support for non-4k systems
	python ../files/widevine_fixup.py opt/WidevineCdm/_platform_specific/linux_arm64/libwidevinecdm_real.so libwidevinecdm.so
	mkdir opt/WidevineCdm/chromium
	mv libwidevinecdm.so opt/WidevineCdm/chromium/libwidevinecdm.so
	eapply_user
}

src_install() {
	mv */ "${D}"
	insinto /opt/WidevineCdm/chromium
	doins manifest.json
	insinto /usr/$(get_libdir)/firefox/browser/defaults/preferences
	doins widevine.js
	dobin ../files/register_widevine_*
}

pkg_postinst() {
	einfo ":: IMPORTANT: In order to register the widevine component with chromium and/or firefox"
	einfo "   run the relevant scripts below for every user that needs widevine enabled."
	einfo "       - register_widevine_firefox"
	einfo "       - register_widevine_chromium"
	einfo "   These scripts will add the needed entries into the user dirs for either"
	einfo "   browser.  This action has to be performed only once; the registering will"
	einfo "   persist after package upgrades."
	einfo ":: IMPORTANT (2): WidevineCDM on ARM now requires patches to glibc.  Therefore"
	einfo "   a dependency to the AUR package glibc-widevine has been added."
}
