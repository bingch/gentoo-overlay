# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake systemd udev
DESCRIPTION="Spacemit K1x Cam Process Platform"
HOMEPAGE="https://gitee.com/bianbu-linux/k1x-cam"

LICENSE="BSD"
SLOT="0"
IUSE=""

if [[ ${PV} == 9999 ]]; then
	EGIT_REPO_URI="https://gitee.com/bianbu-linux/${PN}.git"
    inherit git-r3
else
	SRC_URI="https://gitee.com/bianbu-linux/${PN}/repository/archive/v${PV} -> ${PN}-v${PV}.zip"
    KEYWORDS="~riscv"
	S=${WORKDIR}/${PN}-v${PV}
fi

PATCHES=(
	${FILESDIR}/fix-gcc14.patch
)

src_prepare() {
	eapply_user
	#fix path
	sed -i -e "s/usr\/bin/bin/" sensors/test/CMakeLists.txt || die
	cmake_src_prepare
}

src_install() {
	cmake_src_install
	doinitd etc/init.d/*
	insinto /etc/opt/
	doins etc/opt/*

	systemd_dounit usr/lib/systemd/system/*
	udev_dorules usr/lib/udev/rules.d/*
	# symlink pkgconfig to /usr/lib64/pkgconfig
	mkdir -p ${D}/usr/lib64/pkgconfig || die
	cd ${D}/usr/lib64/pkgconfig; ln -s ../../lib/pkgconfig/*.pc . || die
}

pkg_postinst() {
	systemd_enable_service --global camera
}
